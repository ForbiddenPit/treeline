#!/usr/bin/python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

import argparse
import subprocess
import os
import sys

import time
import re
import glob

print('Welcome in the TreeLine tool')

input('Enter path of working directory\n', work_dir)
input('Enter name of reference dataset (if multi-markers, ref is the metagenomics dataset you want to analyse)\n', ref_name)
input('Enter name of environment dataset (if multi-markers, enter "none")\n', env_name)
input('Maximum number of threads to use at the same time\n', threads)
input('Maximum RAM (memory) to use\n', memory)
input('Alignment type (n for nucleotides, p for proteins)\n', align_type)
input('Alignment program (blast, diamond or mmseqs2)\n', align_prog)
input('Clustering program (cd-hit or linclust)\n', clust_prog)
input('Familydetector threshold to use for the calculation of CCs (95 = genus, recommended)\n', familydetector)
input('Taxonomy limit to use: the percentage of taxons represented by multiple sequences/genes\n', taxo_limit)
input('')

make_config =subprocess.Popen('python3 scripts/make_config.py -w {} -d ./ -r {} -e {} -t {} -m {} --align_type {} --align_prog {} --clust_prog {} -fd {} -tl {} -a {} -k {} -n {}'.format(), stdout=subprocess.DEVNULL, shell=True)
make_config.wait()

snakemake =subprocess.Popen('snakemake --snakefile {} --configfile {}'.format(), stdout=subprocess.DEVNULL, shell=True)
snakemake.wait()