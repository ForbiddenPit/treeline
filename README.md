# Software installation (GitLab project and Conda environment)

For portability, I use Conda: a package, dependency and environment manager that can be installed on Windows, MaxOS or Linux (https://docs.conda.io/projects/conda/en/latest/). The user first needs to install Conda (or Miniconda, a lighter version) on their computer. An environment file (TreeLine.yml) is included in the source files in the ‘conda’ folder: the file contains all the dependencies and software needed for the usage of the pipeline (e.g. BLAST, Snakemake…). When imported in Conda, this file helps setting up the environment for the launch of TreeLine. The commands to install the environment and use it are:

# Installation:
Clone project on computer  
`conda env create -f conda/environment.yml`

## Environment usage:
`conda activate TreeLine`

# Configuration script usage (make_config.py)

Before running the pipeline, the user needs to launch a python script named ‘make_config.py’, which will generate the needed folders such as the working directory and all subdirectories for the pipeline; as well as generating a configuration file for a specific work, containing all the parameters used in the pipeline. This step is mandatory and it will ensure that the user’s computer will be prepared for the pipeline workflow. Those parameters are described in the TreeLine readme and are set when launching the make_config.py script in a console.  
This command example is followed by a brief description of the most important parameters:  

`python3 make_config.py -w [working_dir] -d [dev_dir] -t [number_of_threads] -f [max_number_of_files] -m [max_ram_allocation] -r [reference_data] -e [environmental_data] --alignment_type [n|a] -fd [pid_threshold] -a [af_method] -k [kmers] -n [neighborhood]`

## Directories parameters:
-w [working_dir]: path to a directory chosen by the user. Configuration (config), data and results folders will be created in this working directory.  
-d [dev_dir]: path to the root of the TreeLine folder downloaded from GitLab.

## Computing parameters:
-t [number_of_threads]: maximum number of threads/cores to allocate.  
-f [max_number_of_files]: total number of files in the ref plus the env data folders.  
-m [max_ram_allocation]: maximum amount of RAM to allocate.  

## Data name parameters:
-r [reference_data]: name of the reference data folder to create (e.g. greengenes).  
-e [environmental_data]: name of the environmental data folder to create (e.g. landfill).  

## Programs parameters:
--alignment_type [n|a]: type of BLAST to process and alphabet to use in jD2Stat: n for nucleotides; a for amino-acids.  
-fd [pid_threshold]: PID threshold for the ‘FamilyDetector’ step (described hereafter). 95% is default, corresponding to the genus level.  
-a [af_method]: alignment-free method of the user’s choosing: D2, depending on the usage (D2n) or not (D2S) of neighborhood (see -n parameter).  
-k [kmers]: length of the k-mers to use in the alignment-free method. Recommended is 5, corresponding to a word length of 5 nucleotides/amino acids.  
-n [neighborhood]: size of the k-mer neighborhood. Recommended is 1 for nucleotides; and 0 for amino-acids.  

For parameters such as fd=95, k=5 and n=1: the following configuration file is produced: [working_dir]/config/greengenes-landfill/fd95_k5_n1.yaml (e.g. for the greengenes-landfill project). This configuration file is then used to run the pipeline for this specific project.  
The data folders are created as such: [working_dir]/data/[greengenes]/ and [working_dir]/data/[landfill]/.  
Three results subdirectories are also created: [working_dir]/results/ref/[greengenes]/, [working_dir]/results/env/[landfill]/ and [working_dir]/results/common/[greengenes-landfill]/.  

# Launching the pipeline

After the ‘make_config’ step, the user needs to put each input fasta in either the env or ref data folders. The input files can be in any number, but need to be in the .fasta format. The bash command to run the pipeline uses the Snakemake program:

`snakemake [-p] [-q] [-n] [-j cores] --snakefile [dev_dir]/treeline --config [env=0|1] --configfile [path_to_config]`

## Main parameters:
--snakefile [path_to_snakefile]: path to the snakefile (named treeline and located in the dev directory).  
--configfile [path_to_config]: path to the config file generated at the ‘make_config’ step (e.g. [working_dir]/config/greengenes-landfill/fd95_k5_n1.yaml for the greengenes-landfill project).  
--config [env=0|1]: With env=0, only the reference is processed through the pipeline. With 1, both the reference and environmental data are processed. If -j is set, ref and env can be parallelized (see optional parameters).  

## Optional parameters:
[-p]: print the bash standard output of each program in the console while running.  
[-q]: don’t print the commands launched at each step of the pipeline.  
[-n]: launch a dry run verifying all the steps can be done. Remove to run the pipeline.  
[-j cores]: by default, Snakemake attributes any number of cores needed by each step. Set this option at any number to fix the limit of cores to use by the pipeline. When set, every step that can be parallelized, is, meaning the ref and env data can be processed in parallel to reduce the running time. Only use this option when the computer has enough RAM and cores.  