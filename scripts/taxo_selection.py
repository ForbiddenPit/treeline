#!/usr/bin/python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

import argparse
import subprocess
import os
import sys

import time
import re
import glob
from Bio import SeqIO
from collections import defaultdict

parser = argparse.ArgumentParser(description='Script for alignment all vs all')

# Global arguments (start and end)
## I/O
parser.add_argument('-fg', help='input factorgraph file', required=True)
parser.add_argument('-trail', help='input trail file', required=True)
parser.add_argument('-dict', help='input dict file', required=True)
parser.add_argument('-fasta', help='input fasta file', required=True)
parser.add_argument('-cov', help='set \% of taxonomical coverage (default: 90%)', default='90', required=False, type=float)

args = parser.parse_args()

seq_taxo = dict()
taxo_ids = set()
with open(args.dict) as d:
	for line in d:
		line = line.rstrip()
		ls = line.split()
		seq_taxo[ls[0]] = [ls[1]]
		taxo_ids.add(ls[1])

trail_dict = dict()
seq_taxo_cc = seq_taxo
with open(args.trail) as t:
	for line in t:
		line = line.rstrip()
		ls = line.split()
		if ls[0] in seq_taxo_cc:
			seq_taxo_cc[ls[0]].append(ls[1])

total_taxo = len(taxo_ids)
cc_count = defaultdict(int)
with open(args.fg) as fg:
	for line in fg:
		line = line.rstrip()
		ls = line.split()
		cc_count[ls[0]] += 1

taxo_limit = args.cov
filtered_cc = list()
for cc in cc_count:
	if (cc_count[cc] * 100 / total_taxo) >= taxo_limit:
		filtered_cc.append(cc)

new_fasta = defaultdict(lambda:'')
with open(args.fasta) as f:
	for record in SeqIO.parse(f, 'fasta'):
		cc = seq_taxo_cc[record.id][1]
		taxo = seq_taxo_cc[record.id][0]
		if cc in filtered_cc:
			new_fasta[taxo] = new_fasta[taxo] + str(record.seq)

with open(os.path.dirname(args.fg)+'/CC.fasta', 'w') as output:
	for taxo in new_fasta:
		output.write('>'+taxo+'\n'+new_fasta[taxo]+'\n')

print('done')