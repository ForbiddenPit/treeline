#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import re
import argparse
import numpy as np
from faker import Faker
import matplotlib.colors as mptcol
from matplotlib.colors import LinearSegmentedColormap

parse = argparse.ArgumentParser()

parse.add_argument("--fasta", type=str, help="name of the fasta file",required=False)
parse.add_argument("--annot", type=str, help="name of the annotation file", required=False)
parse.add_argument("--phyla_colors", type=str, help="name of the phyla colors file", required=False)
parse.add_argument("--classes_colors", type=str, help="name of the classes colors file", required=False)

parse.add_argument("--taxo", type=str, help="name of the taxonomy file", required=False)

parse.add_argument("--abund", type=str, help="name of the abundancy file", required=False)

parse.add_argument("--gc_cont", type=str, help="name of the gc content file", required=False)

args = parse.parse_args()

def int_to_hexa(x):
   assert 0 <= x <= 255
   alpha = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]
   v = int(x / 16)
   b = int(x % 16)
   # print(v,b)
   return "{}{}".format(alpha[v], alpha[b])

def hexa_to_int(x):
   alpha = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]
   a = alpha.index(x[0])
   b = alpha.index(x[1])
   return a * 16 + b

def hexa__matpl(color):
   color = color.strip('#').lower()
   r1 = round(hexa_to_int(color[0:2]) / 255, 3)
   r2 = round(hexa_to_int(color[2:4]) / 255, 3)
   r3 = round(hexa_to_int(color[4:]) / 255, 3)
   return (r1, r2, r3)

# print(colors_map(256))
# print(hexa__matpl("#614b3a"))

def generate_colors(): # --taxo
	
	phyla = set()
	classes = set()
	with open(args.taxo) as wd:
		for line in wd:
			ls = line.rstrip('\n').split('\t')
			if len(ls) > 2:
				taxonomy = ls[2].split(';')
				if len(taxonomy) == 2:
					phylum = taxonomy[1].replace('p__', '')
					phyla.add(phylum)
					classes.add('Unknown')
				elif len(taxonomy) > 2:
					phylum = taxonomy[1].replace('p__', '')
					_class = taxonomy[2].replace('c__', '')
					if phylum == '':
						phyla.add('Unknown')
						classes.add('Unknown')
						pass
					elif _class == '':
						phyla.add(phylum)
						classes.add('Unknown')
						pass
					else:
						if re.search(r'(bacterium|archaeon)', _class):
							_class = re.match(r"(.*?)+(bacterium|archaeon)", _class).group()
							if re.search(r'TPA_asm: ', _class):
								_class = _class.replace('TPA_asm: ', '')
							classes.add(_class)
						elif re.search(r'Candidate division TM7', _class):
							_class = 'Candidate division TM7 bacterium'
							classes.add(_class)
						else:
							classes.add(_class)
						phyla.add(phylum)
				else:
					phyla.add('Unknown')
					classes.add('Unknown')
			else:
				pass

	phyla = sorted(phyla)
	classes = sorted(classes)

	fake_colors = Faker()
	phyla_colors = dict()
	classes_colors = dict()
	fake_colors.seed(1996)
	
	phyla_output = open('{}/all-merged_phyla-colors.txt'.format(os.path.dirname(args.taxo)), 'w+')
	for phylum in phyla:
		phyla_colors[phylum] = fake_colors.hex_color()
		phyla_output.write(phylum+'\t'+phyla_colors[phylum]+'\n')

	classes_output = open('{}/all-merged_classes-colors.txt'.format(os.path.dirname(args.taxo)), 'w+')
	for _class in classes:
		classes_colors[_class] = fake_colors.hex_color()
		classes_output.write(_class+'\t'+classes_colors[_class]+'\n')


	phyla_output.write('Color_peak'+'\t'+'#000000'+'\n')
	phyla_output.write('Dallol'+'\t'+'#000000'+'\n')
	phyla_output.write('Landfill'+'\t'+'#000000'+'\n')
	phyla_output.write('Tara'+'\t'+'#000000'+'\n')
	phyla_output.write('Hmg'+'\t'+'#000000'+'\n')

	classes_output.write('Color_peak'+'\t'+'#000000'+'\n')
	classes_output.write('Dallol'+'\t'+'#000000'+'\n')
	classes_output.write('Landfill'+'\t'+'#000000'+'\n')
	classes_output.write('Tara'+'\t'+'#000000'+'\n')
	classes_output.write('Hmg'+'\t'+'#000000'+'\n')

	# bac_map = LinearSegmentedColormap.from_list(
	# 	"bac", [hexa__matpl("#aeffa3"),  hexa__matpl("#59b74d"),  hexa__matpl("#227c16"), hexa__matpl("#0c5e00")])
	# arc_map = LinearSegmentedColormap.from_list(
	# 	"arc", [hexa__matpl("#fc9797"),  hexa__matpl("#b54444"),  hexa__matpl("#e51212"), hexa__matpl("#750000")])
	# cpr_map = LinearSegmentedColormap.from_list(
	# 	"cpr", [hexa__matpl("#fc9797"),  hexa__matpl("#b54444"),  hexa__matpl("#e51212"), hexa__matpl("#750000")])
	# dpann_map = LinearSegmentedColormap.from_list(
	# "dpann", [hexa__matpl("#fc9797"),  hexa__matpl("#b54444"),  hexa__matpl("#e51212"), hexa__matpl("#750000")])

	# cpr_count = 0
	# dpann_count = 0

	# for k, g in new_taxonomy:
	# 	if k == 'Bacteria':
	# 		cpr_count += 1
	# 	elif k == 'Archaea':
	# 		dpann_count += 1
	# print('bact: '+str(cpr_count)+'\tarch: '+str(dpann_count))

	# bac_color = np.linspace(0, 1, cpr_count)
	# cpr_count = 0
	# arc_color = np.linspace(0, 1, dpann_count)
	# dpann_count = 0

	# output = open('{}/all-merged_phylum-colors.txt'.format(os.path.dirname(args.taxo)), 'w+')
	# for k, g in new_taxonomy:
	# 	if k == 'Bacteria':
	# 		phylum_colors[g] = mptcol.rgb2hex(bac_map(bac_color[cpr_count]))
	# 		cpr_count += 1
	# 	elif k == 'Archaea':
	# 		phylum_colors[g] = mptcol.rgb2hex(arc_map(arc_color[dpann_count]))
	# 		dpann_count += 1
	# 	elif k == 'Unknown':
	# 		phylum_colors[g] = '#8c8c8c'
	# 	else:
	# 		phylum_colors[g] = '#f200ff'
	# 	output.write(g+'\t'+phylum_colors[g]+'\n')

def generate_annotation():
	#Chose colors for each "phylum" you want to highlight in the tree
	kingdoms_colors = {"":"#ffffff", "Silva":"#a83295", "Bacteria":"#0000ff", "Archaea":"#ff0000", "Eukaryota":"#008000", "Dallol":"#000000", "Hmg":"#000000", "Color_peak":"#000000", "Landfill":"#000000", "Unknown":"#000000", "Tara":"#000000", "C_lakes":"#000000"}
	phyla_colors = {}
	classes_colors = {}
	dataset_colors = {"":"#ffffff", "CPR":"#00fff2", "DPANN":"#ff5900"}

	kingdoms_dict = {}
	phyla_dict = {}
	classes_dict = {}
	dataset_dict = {}

	with open(args.fasta) as ffh: #Reading the fasta file to find out what nodes are present
		for line in ffh:
			line = line.strip()
			if line.startswith(">"):
				indent = line.replace(">", "").split("\t")[0].split("::")[0]
				kingdoms_dict[indent]=""

	with open(args.phyla_colors) as col:
		for line in col:
			ls = line.rstrip('\n').split('\t')
			phyla_colors[ls[0]] = ls[1]

	with open(args.classes_colors) as col:
		for line in col:
			ls = line.rstrip('\n').split('\t')
			classes_colors[ls[0]] = ls[1]

	with open(args.annot) as fh: #Reading the annotation
		for line in fh:
			line = line.strip()
			split_line = line.split("\t")
			indent = split_line[0]
			kingdom = split_line[1]
			phylum = split_line[2]
			if re.search(r'(bacterium|archaeon)', split_line[3]):
				_class = re.match(r"(.*?)+(bacterium|archaeon)", split_line[3]).group()
			else:
				_class = split_line[3]
			if re.match('CPR', split_line[4]) or re.match('DPANN', split_line[4]):
				dataset = split_line[4]
			else:
				dataset = ""

			if indent in kingdoms_dict:
				kingdoms_dict[indent] = kingdom
				if kingdom == "":
					print(indent)
				phyla_dict[indent] = phylum
				classes_dict[indent] = _class
				dataset_dict[indent] = dataset

	# with open(args.fasta+".itol.dataset_colorstrips.txt","w") as fh:	#output a "colorstrips" format file for itol defining colors. Names MUST end in .txt 
	# 	fh.write("DATASET_COLORSTRIP\nSEPARATOR TAB\nCOLOR_BRANCHES\t0\nDATASET_LABEL\tdataset_colors\nCOLOR\t#ff0000\nDATA\n")
	# 	for sequence in dataset_dict:
	# 		fh.write(sequence.replace("|","_") + "\t" + dataset_colors[dataset_dict[sequence]] + "\t" + dataset_dict[sequence]+"\n")

	with open(args.fasta+".itol.kingdoms_colorstrips.txt","w") as fh:	#output a "colorstrips" format file for itol defining colors. Names MUST end in .txt 
		fh.write("DATASET_COLORSTRIP\nSEPARATOR TAB\nCOLOR_BRANCHES\t0\nDATASET_LABEL\tkingdoms_colors\nCOLOR\t#ff0000\nDATA\n")
		for sequence in kingdoms_dict:
			fh.write(sequence.replace("|","_") + "\t" + kingdoms_colors[kingdoms_dict[sequence]] + "\t" + kingdoms_dict[sequence]+"\n")

	# with open(args.fasta+".itol.phyla_legend.txt","w") as fh:	#output a "colorstrips" format file for itol defining colors. Names MUST end in .txt 
	# 	fh.write("DATASET_COLORSTRIP\nSEPARATOR TAB\nCOLOR_BRANCHES\t0\nDATASET_LABEL\tphyla_legend\nCOLOR\t#ff0000\nLEGEND_TITLE\tPhyla\nLEGEND_SHAPES{}\nLEGEND_COLORS".format(len(phyla_colors)*"\t2"))
	# 	for phylum in phyla_colors:
	# 		fh.write('\t'+phyla_colors[phylum])
	# 	fh.write("\nLEGEND_LABELS")
	# 	for phylum in phyla_colors:
	# 		fh.write('\t'+phylum)
	with open(args.fasta+".itol.phyla_colorstrips.txt","w") as fh:	#output a "colorstrips" format file for itol defining colors. Names MUST end in .txt 
		fh.write("DATASET_COLORSTRIP\nSEPARATOR TAB\nCOLOR_BRANCHES\t0\nDATASET_LABEL\tphyla_colors\nCOLOR\t#ff0000\nDATA\n")
		for sequence in phyla_dict:
			fh.write(sequence.replace("|","_") + "\t" + phyla_colors[phyla_dict[sequence]] + "\t" + phyla_dict[sequence]+"\n")
	

	# with open(args.fasta+".itol.classes_legend.txt","w") as fh:	#output a "colorstrips" format file for itol defining colors. Names MUST end in .txt 
	# 	fh.write("DATASET_COLORSTRIP\nSEPARATOR TAB\nCOLOR_BRANCHES\t1\nDATASET_LABEL\tclasses_legend\nCOLOR\t#ff0000\nLEGEND_TITLE\tClasses\nLEGEND_SHAPES{}\nLEGEND_COLORS".format(len(classes_colors)*"\t1"))
	# 	for _class in classes_colors:
	# 		fh.write('\t'+classes_colors[_class])
	# 	fh.write("\nLEGEND_LABELS")
	# 	for _class in classes_colors:
	# 		fh.write('\t'+_class)
	# 	fh.write("\nDATA\n")
	with open(args.fasta+".itol.classes_colorstrips.txt","w") as fh:	#output a "colorstrips" format file for itol defining colors. Names MUST end in .txt 
		fh.write("DATASET_COLORSTRIP\nSEPARATOR TAB\nCOLOR_BRANCHES\t1\nDATASET_LABEL\tclass_colors\nCOLOR\t#ff0000\nDATA\n")
		for sequence in classes_dict:
			fh.write(sequence.replace("|","_") + "\t" + classes_colors[classes_dict[sequence]] + "\t" + classes_dict[sequence]+"\n")


	# with open(args.fasta+".itol.labels.txt","w") as fh: #output a "labels" format file for itol that replaces IDs with genome names. Names MUST end in.txt
	# 	fh.write("LABELS\nSEPARATOR TAB\nDATA\n")
	# 	for sequence in kingdoms_dict:
	# 		fh.write(sequence.replace("|","_")  + "\t"+ sequence.replace("|","_")+"\n")

# def generate_gc_annot():

# 	gc_map = LinearSegmentedColormap.from_list(
#       "gc_map", [(0, 0.8, 0.8),  (0, 0.25, 0),  (0, 0.8,0), hexa__matpl("#614b3a")])

# 	with open(args.gc_cont) as gcc:
# 		gc_contents = {}
# 		for line in gcc:
# 			line = line.rstrip('\n')
# 			ls = line.split('\t')
# 			print(colors_map(int(ls[1])))
# 			# gc_contents[ls[0]] = colors_map(int(ls[1]))
# 			# print(gc_contents[ls[0]])

# 	with open(args.annot) as fh: #Reading the annotation
# 		for line in fh:
# 			line = line.strip()
# 			split_line = line.split("\t")
# 			indent = split_line[0]
# 			dataset = split_line[3]

# 			if indent in gc_contents:
# 				dataset_dict[indent] = dataset

	# with open(args.fasta+".itol.kingdomss_colorstrips.txt","w") as fh:	#output a "colorstrips" format file for itol defining colors. Names MUST end in .txt 
	# 	fh.write("DATASET_COLORSTRIP\nSEPARATOR TAB\nCOLOR_BRANCHES\t1\nDATASET_LABEL\tkingdoms_colors\nCOLOR\t#ff0000\nDATA\n")
	# 	for sequence in kingdoms_dict:
	# 		fh.write(sequence.replace("|","_") + "\t" + kingdoms_colors[kingdoms_dict[sequence]] + "\t" + kingdoms_dict[sequence]+"\n")

def generate_abundancy():

	abund_map = LinearSegmentedColormap.from_list(
      "abund_map", [hexa__matpl("#ffd1d1"), hexa__matpl("#ff0000")])
	
	with open(args.abund) as ab:
		abundancy_color = {}
		abundancy_score = {}
		for line in ab:
			line = line.rstrip('\n')
			ls = line.split('\t')
			abundancy_color[ls[0]] = mptcol.rgb2hex(abund_map(int(ls[1])))
			# abundancy_score[ls[0]] = ls[1]
			abundancy_score[ls[0]] = (str(np.log10(int(ls[1]))), ls[1])

	with open(args.abund+".itol.abundancy_colorstrips.txt","w") as fh:	#output a "colorstrips" format file for itol defining colors. Names MUST end in .txt 
		fh.write("DATASET_COLORSTRIP\nSEPARATOR TAB\nCOLOR_BRANCHES\t0\nDATASET_LABEL\tabundancy_colors\nCOLOR\t#ff0000\nDATA\n")
		for sequence in abundancy_color:
			fh.write(sequence.replace("|","_") + "\t" + str(abundancy_color[sequence]) + "\t" + str(abundancy_score[sequence]) + "\n")

	with open(args.abund+".itol.abundancy_simplebar.txt","w") as fh:	#output a "colorstrips" format file for itol defining colors. Names MUST end in .txt 
		fh.write("DATASET_SIMPLEBAR\nSEPARATOR TAB\nDATASET_LABEL\tabundancy_barplot\nCOLOR\t#ff0000\nDATA\n")
		for sequence in abundancy_color:
			fh.write(sequence.replace("|","_") + "\t" + str(abundancy_score[sequence][0]) + "\t" + str(abundancy_score[sequence][1]) + "\n")


if args.taxo:
	generate_colors()

elif args.fasta and not args.abund:
	generate_annotation()

elif args.gc_cont:
	generate_gc_annot()

elif args.abund:
	generate_abundancy()

else:
	print('No valid args')
