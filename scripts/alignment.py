#!/usr/bin/python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

import argparse
import subprocess
import os
import sys

import time
import re
import glob
from multiprocessing import Pool
from collections import OrderedDict
from collections import defaultdict

parser = argparse.ArgumentParser(description='Script for alignment all vs all')

# Global arguments (start and end)
## I/O
parser.add_argument('--fasta', help='input fasta', required=True)
parser.add_argument('--output', help='outfile', required=False)
## Software choice
parser.add_argument('--align_prog', help='alignment program used for alignment', required=True)
parser.add_argument('--align_type', help='alignment type (nucleotide or amino-acid)', required=True)
## Software parameters
parser.add_argument('--threads', help='number of threads to use for alignment, selected by the user', default=1, required=True)
## Database type
parser.add_argument('--dbtype', help='database type (paired with align_type: N or A)', required=True)
## Working directory path
parser.add_argument('--work_dir', help='working directory path', default='./', required=True)
## Dev dir to access programs
parser.add_argument('--dev_dir', help='dev directory (treeline folder root)', required=True)

args = parser.parse_args()

def split():
	
	print('Splitting query fasta\n')
	
	# Create split folder if doesn't exist
	if not os.path.exists(args.work_dir+"split/"):
		os.makedirs(args.work_dir+"split/")

	# Fastasplit
	child = subprocess.Popen('fastasplit -f {} -o {}split/ -c {}'.format(args.fasta, args.work_dir, nb_splits), stdout=subprocess.DEVNULL, shell=True)
	child.wait()

	# Find newly created splits
	files = glob.glob(args.work_dir+'split/*.fasta_*')

	return files

	print('Split done: fastasplit used to divide query fasta')

def preprocessing(prog):

	print('Preprocessing...\n')

	if not os.path.exists(args.work_dir+"db/"):
		os.makedirs(args.work_dir+"db/")

	if prog == 0: # Blast preprocessing
		# Make the required database for chosen program
		dbtype = args.dbtype
		child =subprocess.Popen('makeblastdb -in {} -dbtype {} -out {}db/merged_database'.format(args.fasta, dbtype, args.work_dir), stdout=subprocess.DEVNULL, shell=True)
		child.wait()

		return dbtype

	elif prog == 1: # Diamond preprocessing
		# Make the required database for chosen program
		dbtype = args.dbtype
		child =subprocess.Popen('diamond makedb --in {} --db {}db/merged_database'.format(args.fasta, args.work_dir), stdout=subprocess.DEVNULL, shell=True)
		child.wait()

		return dbtype

	elif prog == 2: # MMseqs2 preprocessing
		if args.dbtype == "nucl":
			dbtype = 2
			search_type = 3
		elif args.dbtype == "prot":
			dbtype = 1
			search_type = 1
		else:
			print("Wrong database type selected, set to automatic (0).\nIf the fasta type is not recognized correctly, check the config file (dbtype line)")
			dbtype = 0
		child = subprocess.Popen('{}/scripts/MMseqs2/build/bin/mmseqs createdb {} {}db/merged_database --dbtype {} '.format(args.dev_dir, args.fasta, args.work_dir, dbtype), stdout=subprocess.DEVNULL, shell=True)
		child.wait()

		return dbtype, search_type

	print('Preprocessing done: database created\n')

def blast(tuple_args):

	file, outfile, file_count, nb_splits = tuple_args
	blst = 'blast{} -query {} -db {}db/merged_database -out {} -outfmt \"6 qseqid sseqid evalue pident bitscore qstart qend qlen sstart send slen\" -num_threads 4 -max_target_seqs 5000 -soft_masking false'.format(args.align_type, file, args.work_dir, outfile)
	try:
		child = subprocess.Popen(blst, stdout=subprocess.DEVNULL, shell=True)
		child.wait()
	except:
		raise
	else:
		return 0

def diamond(tuple_args):

	file, outfile, file_count, nb_splits = tuple_args
	dmnd = 'diamond blastp --query {} --db {}db/merged_database --out {} --outfmt 6 qseqid sseqid evalue pident bitscore qstart qend qlen sstart send slen --threads {} --max-target-seqs 5000'.format(file, args.work_dir, outfile, nb_splits)
	try:
		child = subprocess.Popen(dmnd, stdout=subprocess.DEVNULL, shell=True)
		child.wait()
	except:
		raise
	else:
		return 0

def mmseqs2(tuple_args):

	file, outfile, file_count, nb_splits = tuple_args
	srch = '{}scripts/MMseqs2/build/bin/mmseqs easy-search {} {}db/merged_database {} {}tmp{} -s 7.5 --search-type {} --dbtype {} --format-output \"query,target,evalue,pident,bits,qstart,qend,qlen,tstart,tend,tlen\" --threads {} --filter-msa 0 --alignment-mode 3'.format(args.dev_dir, file, args.work_dir, outfile, args.work_dir, file_count, search_type, dbtype, nb_splits)
	try:
		child = subprocess.Popen(srch, stdout=subprocess.DEVNULL, shell=True)
		child.wait()
	except:
		raise
	else:
		return 0

# Check number of sequences before split attribution and use number of splits to determine the number of threads used in blast
n_seq = int(subprocess.check_output("grep -c '>' {}".format(args.fasta), shell=True).decode('utf-8'))

if int(args.threads) < n_seq:
	nb_splits = int(args.threads)//4
	if nb_splits < 4:
		nb_splits = 1
else:
	nb_splits = n_seq

# Split fasta
files = split()

# pool.map: we create arguments for each file
list_args = defaultdict(list)
file_count = 0
split_step = 0 # Keep track of nb of files to split multiprocessing in 5 files at a time
for file in files:
	tuple_args = (file, file.replace('.fasta', '.out'), str(file_count), nb_splits)
	list_args[split_step].append(tuple_args)
	file_count += 1
	if (file_count % 4) == 0:
		split_step += 1

if args.align_prog == "blast":
	# Make database
	dbtype = preprocessing(0)
	for step in list_args:

		print('Launching blast on splitted files\n')
		
		# Create pool
		with Pool(processes=nb_splits) as pool:
			# Launch blast in parallel for all files
			out = pool.map(blast, list_args[step])

	print('Blast succeeded\n')

elif args.align_prog == "diamond":
	# Make database
	dbtype = preprocessing(1)
	for step in list_args:

		print('Launching diamond on splitted files\n')
		
		# Create pool
		with Pool(processes=nb_splits) as pool:
			# Launch blast in parallel for all files
			out = pool.map(diamond, list_args[step])

	print('Diamond succeeded\n')

elif args.align_prog == "mmseqs2":
	# Make database
	dbtype, search_type = preprocessing(2)
	for step in list_args:

		print('Launching mmseqs2 on splitted files\n')
		
		# Create pool
		with Pool(processes=nb_splits) as pool:
			# Launch blast in parallel for all files
			out = pool.map(mmseqs2, list_args[step])

	print('MMseqs2 succeeded\n')

	# Remove tmp directory

	print('Removing tmp directories\n')

	child = subprocess.Popen("rm -r {}tmp*/".format(args.work_dir), shell=True)
	child.wait()

else:
	print('No action done, verify input and options')

# Concatenate results
print('Concatenating out files...\n')
child = subprocess.Popen("cat {}split/*.out_* > {}".format(args.work_dir, args.output), shell=True)
child.wait()

# Remove split directory
print('Removing split directory\n')
child = subprocess.Popen("rm -r {}split/".format(args.work_dir), shell=True)
child.wait()

# Remove db directory
print('Removing database directory\n')
child = subprocess.Popen("rm -r {}db/".format(args.work_dir), shell=True)
child.wait()