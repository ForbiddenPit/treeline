# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/lib/ksw2/ksw2_extz2_sse.cpp" "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/lib/ksw2/CMakeFiles/ksw2.dir/ksw2_extz2_sse.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib/tinyexpr"
  "../lib"
  "../lib/kseq"
  "../lib/simd"
  "../lib/gzstream"
  "../lib/alp"
  "../lib/cacode"
  "../lib/ksw2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
