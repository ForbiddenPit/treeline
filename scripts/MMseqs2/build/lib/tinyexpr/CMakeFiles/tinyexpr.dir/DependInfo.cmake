# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/lib/tinyexpr/tinyexpr.c" "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/lib/tinyexpr/CMakeFiles/tinyexpr.dir/tinyexpr.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "TE_NAT_LOG"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../lib/tinyexpr"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
