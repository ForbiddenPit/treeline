# Install script for directory: /home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RELEASE")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/mmseqs" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/mmseqs")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/mmseqs"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/mmseqs")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/mmseqs" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/mmseqs")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/mmseqs")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/alignment/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/clustering/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/commons/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/linclust/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/multihit/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/prefiltering/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/taxonomy/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/util/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/workflow/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/version/cmake_install.cmake")
  include("/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/test/cmake_install.cmake")

endif()

