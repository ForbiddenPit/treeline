# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/src/version/Version.cpp" "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/version/CMakeFiles/version.dir/Version.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GIT_SHA1=2b206ea283ff73642009fe8b1758bb83723f45f7"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib/tinyexpr"
  "../lib"
  "../lib/kseq"
  "../lib/simd"
  "../lib/gzstream"
  "../lib/alp"
  "../lib/cacode"
  "../lib/ksw2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
