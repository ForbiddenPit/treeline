# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/src/test/TestAlignmentPerformance.cpp" "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/test/CMakeFiles/test_alignmentperformance.dir/TestAlignmentPerformance.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "AVX2=1"
  "HAVE_BZLIB=1"
  "HAVE_POSIX_FADVISE=1"
  "HAVE_POSIX_MADVISE=1"
  "HAVE_ZLIB=1"
  "OPENMP=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib/tinyexpr"
  "../lib"
  "../lib/kseq"
  "../lib/simd"
  "../lib/gzstream"
  "../lib/alp"
  "../lib/cacode"
  "../lib/ksw2"
  "generated"
  "../src/alignment"
  "../src/clustering"
  "../src/commons"
  "../src/multihit"
  "../src/prefiltering"
  "../src/linclust"
  "../src/taxonomy"
  "../src/util"
  "../src/."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/CMakeFiles/mmseqs-framework.dir/DependInfo.cmake"
  "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/lib/zstd/build/cmake/lib/CMakeFiles/libzstd_static.dir/DependInfo.cmake"
  "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/src/version/CMakeFiles/version.dir/DependInfo.cmake"
  "/home/pierre/Documents/2019_internship/dev/treeline/scripts/MMseqs2/build/lib/tinyexpr/CMakeFiles/tinyexpr.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
