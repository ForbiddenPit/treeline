#!/usr/bin/python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

import os
import argparse
import re
import sys
import glob
from Bio import SeqIO

# Calling arguments from console when using the script
parser = argparse.ArgumentParser()

parser.add_argument('-fd', '--fasta_dir', metavar='DIR', help='Fasta directory. Default: none.', default='', dest='fasta_dir')
parser.add_argument('-t', '--taxo_dict', metavar='FILE', help='Taxonomy dictionary. Default: none.', default='', dest='taxo_dict')
parser.add_argument('-o', '--output', metavar='DIR', help='Output directory. Default: none.', default='', dest='output_dir')
parser.add_argument('--data', metavar='STR', help='Initial data type (ref or env). Default: none.', default='', dest='data_type')

args = parser.parse_args()

# Crate new unique fasta ids and a global dictionnary
print('renaming initial fastas...')

taxo_dict = dict()
with open(args.taxo_dict) as t:
	for line in t:
		line = line.rstrip()
		ls = line.split()
		taxo_dict[ls[0]] = ls[1]

if args.data_type == 'ref':
	suffix = '0'
elif args.data_type == 'env':
	suffix = '1'
elif args.data_type == 'none':
	suffix = ''
else:
	sys.exit('please relaunch with the good data type (env or ref or none)')

seq_count = 1
for fasta in glob.glob(args.fasta_dir+'*.fasta'):
	output1 = open('{}/renamed_{}'.format(os.path.dirname(args.output_dir), os.path.basename(fasta)), 'w+')
	output2 = open('{}/renamed_{}'.format(os.path.dirname(args.output_dir), os.path.basename(fasta).replace('.fasta', '.dict')), 'w+')
	output3 = open('{}/renamed_{}'.format(os.path.dirname(args.output_dir), os.path.basename(args.taxo_dict)), 'w+')
	with open(fasta) as f:
		for record in SeqIO.parse(f, 'fasta'):
			output1.write(">"+str(seq_count)+suffix+'\n'+str(record.seq)+'\n')
			output2.write(str(seq_count)+suffix+'\t'+str(record.description)+'\n')
			if record.id in taxo_dict:
				output3.write(str(seq_count)+'\t'+taxo_dict[record.id]+'\n')
			seq_count += 1
	print(os.path.basename(fasta))

print('done')