#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Pierre Martin

Master 2 Internship 2019

Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138
Evolution Paris Seine - Institut de Biologie Paris Seine
Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France
"""

import os
import argparse
import re
import sys
import glob
from Bio import SeqIO

from functions_16s import *

# Calling arguments from console when using the script
parser = argparse.ArgumentParser()
# Adding mandatory arguments:
parser.add_argument('-s', '--swarm',
                    metavar='FILE', help='Swarm file. Default: none.',
                    default='', dest='swarm_file')
parser.add_argument('-f', '--fasta',
                    metavar='FILE', help='Fasta file. Default: none.',
                    default='', dest='fasta_file')
parser.add_argument('-n', '--newick',
                    metavar='FILE', help='Newick file. Default: none.',
                    default='', dest='nwk_file')
parser.add_argument('-d', '--dict',
                    metavar='FILE', help='Dictionary file. Default: none.',
                    default='', dest='dict_file')
parser.add_argument('-cl', '--cluster',
                    metavar='FILE', help='Cluster file. Default: none.',
                    default='', dest='cluster_file')
parser.add_argument('-e', '--edges',
                    metavar='FILE', help='Edges file. Default: none.',
                    default='', dest='edges_file')
parser.add_argument('-t', '--taxo',
                    metavar='FILE', help='Taxonomy file for gg. Default: none.',
                    default='', dest='taxo_file')
parser.add_argument('-t1', '--taxo1',
                    metavar='FILE', help='Taxonomy file for gg. Default: none.',
                    default='', dest='taxo_file1')
parser.add_argument('-t2', '--taxo2',
                    metavar='FILE', help='Taxonomy file for cpr_dpann. Default: none.',
                    default='', dest='taxo_file2')
parser.add_argument('-t3', '--taxo3',
                    metavar='FILE', help='Taxonomy file for 8000 or tara_oceans. Default: none.',
                    default='', dest='taxo_file3')
parser.add_argument('-c', '--count',
                    metavar='FILE', help='Count table file. Default: none.',
                    default='', dest='count_table')

parser.add_argument('--derep_dir',
                    metavar='DIR', help='Derep directory path. Default: none.',
                    default='', dest='derep_dir')
parser.add_argument('--cdhit_dir',
                    metavar='DIR', help='CDHIT directory path. Default: none.',
                    default='', dest='cdhit_dir')
parser.add_argument('--network_dir',
                    metavar='DIR', help='Network directory path. Default: none.',
                    default='', dest='network_dir')

parser.add_argument('-tara', '--tara_fasta',
                    metavar='FILE', help='Tara fasta. Default: none.',
                    default='', dest='tara_fasta')

parser.add_argument('-g', '--gbff',
                    metavar='DIR', help='Genbank directory. Default: none.',
                    default='', dest='gbff_dir')
parser.add_argument('-fd', '--fasta_dir',
                    metavar='DIR', help='Fasta directory. Default: none.',
                    default='', dest='fasta_dir')
parser.add_argument('-o', '--output',
                    metavar='DIR', help='Output directory. Default: none.',
                    default='', dest='output_dir')
parser.add_argument('--data',
                    metavar='STR', help='Initial data type (ref or env). Default: none.',
                    default='', dest='data_type')

# parser.add_argument('-i1', '--id1',
#                     metavar='FILE', help='Identifiers file n°1. Default: none.',
#                     default='', dest='id1')
# parser.add_argument('-i2', '--id2',
#                     metavar='FILE', help='Identifiers file n°2. Default: none.',
#                     default='', dest='id2')
args = parser.parse_args()

# -------------------------------------------------------------------

########
# Main #
########

# Args choice

# Create a fasta with the original ids from before dereplication (swarm file)
if args.swarm_file and args.dict_file:
	dic = open_dict(args.dict_file)
	original_swarm_id(args.swarm_file, dic)

# Create a fasta with the original ids from before dereplication (fasta file)
if args.fasta_file and args.dict_file:
	dic = open_dict(args.dict_file)
	original_fasta_id(args.fasta_file, dic)

# Create a nwk with the original ids from dictionary
if args.nwk_file and args.dict_file:
     dic = open_dict(args.dict_file)
     original_nwk_id(args.nwk_file, dic)

# List nwk ids
if args.nwk_file:
     nwk_ids_list(args.nwk_file)

# Check if vxtractor result has duplicates and keep longest sequence of two
if args.fasta_file and not args.dict_file and not args.derep_dir:
	check_duplicates(args.fasta_file)

# Create new dict for each fasta file in directory
if args.fasta_dir and args.output_dir:
	rename_original_fasta(args.fasta_dir, args.output_dir, args.data_type)

if args.cluster_file:
	cdhit_abundancy(args.cluster_file)

if args.edges_file:
	edges_abundancy(args.edges_file)

# create new count table after cc trimming
if args.fasta_file and args.count_table:
	update_count_table(args.fasta_file, args.count_table)

# Dict + taxo to retrieve taxonomy in ids
if args.dict_file and args.taxo_file:
	dict_taxo(args.dict_file, args.taxo_file)

# Generate abundance
if args.derep_dir and args.cdhit_dir and args.network_dir:
     generate_abundance(args.derep_dir, args.cdhit_dir, args.network_dir, args.fasta_file)

# Create a fasta with blast ids and a corresponding count table
# if args.fasta_file and not args.dict_file and not args.count_table:
# 	fasta_file = args.fasta_file
# 	blast_id(fasta_file)

# greengenes + dallol or color_peak + cpr_dpann + other
# if args.count_table and not args.fasta_file:
# 	all_taxo = dict()
# 	all_lists = dict()
# 	if args.taxo_file1:
# 		all_taxo[0], all_lists[0] = taxo_gg(args.taxo_file1)
# 	if args.taxo_file2:
# 		all_taxo[1], all_lists[1] = taxo_other(args.taxo_file2)
# 	if args.taxo_file3:
# 		all_taxo[2], all_lists[2] = taxo_other(args.taxo_file3)
# 	taxo_network(args.count_table, all_taxo, all_lists)

if args.gbff_dir:
	gbff_dir = args.gbff_dir + '/*.gbff'
	gbff_handling(gbff_dir)

if args.tara_fasta:
	tara_dict(args.tara_fasta)