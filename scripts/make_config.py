#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import yaml
import argparse
import sys
import math

## Main
parser = argparse.ArgumentParser()

# Directories
parser.add_argument('-w', '--workdir', metavar='DIR', help='working directory', default='', required=True, dest='work_dir')
parser.add_argument('-d', '--devdir', metavar='DIR', help='treeline repository', default='', required=True, dest='dev_dir')

# Project name
parser.add_argument('-r', '--ref_name', metavar='STR', help='name of the reference data', default='', required=True, dest='ref_name')
parser.add_argument('-e', '--env_name', metavar='STR', help='name of the environmental data', default='', required=True, dest='env_name')

# Computer params
parser.add_argument('-t', '--threads', metavar='NUM', help='number of threads', default='20', required=False, dest='threads', type=int)
parser.add_argument('-m', '--memory', metavar='NUM', help='maximum virtual memory to allocate', default='100', required=False, dest='max_mem')

# Alignment params
parser.add_argument('--align_type', metavar='STR', help='type of alignment (n or p)', default='n', required=False, dest='align_type')
parser.add_argument('--align_prog', metavar='STR', help='program to use for alignment (blast or mmseqs2)', default='mmseqs2', required=False, dest='align_prog')

# Clustering params
parser.add_argument('--clust_prog', metavar='STR', help='program to use for clustering (cdhit or linclust)', default='linclust', required=False, dest='clust_prog')

# Family Detector params
parser.add_argument('-fd', '--familydetector', metavar='NUM', help='pid threshold for familydetector (95 = genus; 97 = species)', default='95', required=False, dest='familydetector')

# Taxonomical coverage of CCs
parser.add_argument('-tl', '--taxo_limit', metavar='NUM', help='set percentage of taxonomical coverage (default: 90)', default='90', required=False, dest='taxo_limit')

# jD2Stats params
parser.add_argument('-a', '--analysis', metavar='STR', help='k-mers method to use (D2S is advised when n = 0; D2N is advised when n > 0)', default='D2N', required=False, dest='analysis_type')
parser.add_argument('-k', '--kmers', metavar='NUM', help='number of k-mers', default='5', required=False, dest='kmers')
parser.add_argument('-n', '--neighbor', metavar='NUM', help='number of neighbors', default='1', required=False, dest='neighbor')

args = parser.parse_args()

# Alignment specific parameters
align_type = args.align_type
align_prog = args.align_prog

if align_prog == "blast":
    multiplicator = 0
elif align_prog == "mmseqs2":
    multiplicator = 1
elif align_prog == "diamond":
    multiplicator = 0
    align_type = "p"

if align_type == "n":
    alphabet = "dna"
    dbtype = "nucl"
elif align_type == "p":
    alphabet = "aa"
    dbtype = "prot"

# Other options
familydetector = args.familydetector
kmers = args.kmers
neighbor = args.neighbor

# Create working directory
work_dir = os.path.abspath(args.work_dir)
if not os.path.exists(work_dir):
    os.makedirs(work_dir)
else:
    print("Working directory "+work_dir+" already exists")

# Create config directory
ref_name = args.ref_name
env_name = args.env_name
output_dir = work_dir+'/config/'+env_name+'-'+ref_name+'/'
if not os.path.exists(output_dir):
    os.makedirs(output_dir)
else:
    print("Config directory "+output_dir+" already exists")

# Create data directories
env_dir = work_dir+'/data/env/'+env_name+'/'
if not os.path.exists(env_dir):
    os.makedirs(env_dir)
else:
    print("Config directory "+env_dir+" already exists")

ref_dir = work_dir+'/data/ref/'+ref_name+'/'
if not os.path.exists(ref_dir):
    os.makedirs(ref_dir)
else:
    print("Config directory "+ref_dir+" already exists")

# Create results directories
env_res_dir = work_dir+'/results/env/'+env_name+'/'
if not os.path.exists(env_res_dir):
    os.makedirs(env_res_dir)
else:
    print("Config directory "+env_res_dir+" already exists")
ref_res_dir = work_dir+'/results/ref/'+ref_name+'/'

if not os.path.exists(ref_res_dir):
    os.makedirs(ref_res_dir)
else:
    print("Config directory "+ref_res_dir+" already exists")
common_res_dir = work_dir+'/results/common/'+env_name+'-'+ref_name+'/'

if not os.path.exists(common_res_dir):
    os.makedirs(common_res_dir)
else:
    print("Config directory "+common_res_dir+" already exists")

# Create a config file containing every specifics to run Treeline
## Name of the config file to create
output_file = output_dir+'fd'+familydetector+'_k'+kmers+'_n'+neighbor+'.yaml'
## Other specs
dev_dir = os.path.abspath(args.dev_dir)+'/'
tree_params = "_fd"+familydetector+"_k"+kmers+"_n"+neighbor
## Content of the yaml file
config_file = {
"dev_dir": dev_dir,
"env_name": env_name,
"env_dir": env_dir,
"env_res_dir": env_res_dir,
"ref_name": ref_name,
"ref_dir": ref_dir,
"ref_res_dir": ref_res_dir,
"common_res_dir": common_res_dir,
"tree_params": tree_params,
"clustering": {"clust_prog":args.clust_prog, "num_threads":str(args.threads)},
"align": {"align_prog":align_prog, "align_type":align_type, "dbtype":dbtype, "num_threads":str(args.threads)},
"cleanblast": {"multiplicator":multiplicator},
"familydetector": {"p":familydetector, "c":"80", "m":"cc", "o":"1", "t":str(args.threads)},
"taxo_selection": {"taxo_limit":args.taxo_limit},
"jD2Stat": {"max_mem":args.max_mem, "k":kmers, "d":args.analysis_type, "t":str(args.threads), "a":alphabet, "n":neighbor}
}

# Write config yaml with parameters
with open(output_file, 'w') as outfile:
    yaml.dump(config_file, outfile, default_flow_style=False, sort_keys=False)
print("Config file "+os.path.basename(output_file)+" created")