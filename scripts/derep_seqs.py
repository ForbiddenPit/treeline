 #! /usr/bin/env python3
__author__ = "Gene Blanchard"
__email__ = "me@geneblanchard.com"

'''
derep_seqs.py
Python version of USEARCH's derep command
Fixed the time output to H:M:S format
'''
""" ~/Documents/2019_internship/dev/dallol/derep_seqs.py -i 030818KOF515F-pr.fasta """

'''
Modified by
#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#
'''

from optparse import OptionParser
from Bio import SeqIO
import subprocess 
import sys
import os
import time
from datetime import timedelta

start_time = time.time()

# Create the option parser
parser = OptionParser()

# input -i --input
parser.add_option("-i", "--input", action="store", type="string", dest="input_fasta", help="The fasta to dereplicate")
parser.add_option("-f", "--output_fasta", action="store", type="string", dest="output_fasta", help="The dereplicated fasta")
parser.add_option("-d", "--output_dict", action="store", type="string", dest="output_dict", help="The dereplicated dict")

# Grab command line options
(options, args) = parser.parse_args()

ERROR = False
# Check if the input directory exists
if options.input_fasta == None:
	print('Please enter a fasta to dereplicate.\n\tFor more information use the -h option')
	ERROR = True

# Quit on error
if ERROR == True:
	sys.exit()

# Set variables from command line options
input_fasta = options.input_fasta
output_fasta = options.output_fasta
output_dict = options.output_dict

# A hash for the unique sequences
sequences = {}

# Store the original ids in a dict
original_ids = dict()

# Number of lines in the file 
records_process = subprocess.Popen(['wc','-l',input_fasta], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
result, err = records_process.communicate()
if records_process.returncode != 0:
	raise IOError(err)
records = int(result.strip().split()[0]) /2
print('There are %s sequences in the fasta' % records)

# Read in the fasta
duplicate_time = time.time()
print('Checking for duplicates')
sequence_count = 0
total_id = 0
for seq_record in SeqIO.parse(input_fasta, 'fasta'):
	sequence_count = sequence_count + 1
	sequence = str(seq_record.seq)
	if sequence not in sequences:
		sequences[sequence]=str(seq_record.id)+'_1'
		# original_ids[total_id] = [seq_record.description]
		total_id += 1
	else:
		abundance = int(sequences[sequence].split('_')[-1]) + 1
		this_id = sequences[sequence].split('_')[0]
		# original_ids[int(this_id)].append(seq_record.description)
		formated_string = this_id+'_'+str(abundance)
		sequences[sequence] = formated_string
		percent_complete = "%.0f%%" % (100.0 * sequence_count/records)
		sys.stdout.write('Percent complete:\t'+percent_complete+'\r')
		sys.stdout.flush()
duplicate_time_complete = time.time()-duplicate_time
print('It took %s seconds to remove duplicates' % int(duplicate_time_complete))

# Write out our dereplicated fasta
print('Writing the output fasta file: derep_%s' % os.path.basename(input_fasta))
print('and writing the corresponding abundancy dictionary: %s.dict' % os.path.basename(input_fasta).split('.')[0])
with open(output_fasta, 'w+') as out1:
	with open(output_dict, 'w+') as out2:
		for sequence in sequences:
			out1.write('>'+sequences[sequence].split('_')[0]+'\n'+sequence+'\n')
			out2.write(sequences[sequence].split('_')[0]+'\t'+sequences[sequence].split('_')[-1]+'\n')
# 	for new_id in original_ids:
# 		out2.write(str(new_id)+'\t')
# 		for old_id in original_ids[new_id]:
# 			out2.write(str(old_id)+";")
# 		out2.write('\n')

elapsed_time = time.time()-start_time
delta =  timedelta(seconds=elapsed_time)
print('Total time:\t%s' % str(delta))