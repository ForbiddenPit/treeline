#!/usr/bin/python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

import argparse
import subprocess
import os
import sys

import time
import re
import glob
from multiprocessing import Pool
from collections import OrderedDict
from collections import defaultdict

parser = argparse.ArgumentParser(description='Script for alignment all vs all')

# Global arguments (start and end)
## I/O
parser.add_argument('--fasta', help='input fasta', required=True)
parser.add_argument('--outfile', help='outfile', required=False)
## Software choice
parser.add_argument('--clust_prog', help='clustering program used (cdhit or linclust)', required=True)
## Software parameters
parser.add_argument('--threads', help='number of threads to use for alignment split, selected by the user', default=1, required=True)
## Working directory path
parser.add_argument('--work_dir', help='working directory path', default='./', required=True)
## Dev dir to access programs
parser.add_argument('--dev_dir', help='dev directory (treeline folder root)', required=True)

args = parser.parse_args()

def cdhit(fasta, outfile, threads):
	child =subprocess.Popen('{}scripts/cdhit/cd-hit-est -i {} -o {} -T {} -c 0.97 -n 10 -d 0 -M 16000'.format(args.dev_dir, fasta, outfile, threads), stdout=subprocess.DEVNULL, shell=True)
	child.wait()

def linclust(fasta, outfile, threads):
	# Launch linclust
	child = subprocess.Popen('{}scripts/MMseqs2/build/bin/mmseqs easy-linclust {} {} {}tmp --threads {} --alignment-mode 3 --min-seq-id 0.97 --dont-shuffle 0 --sort-results 1 --seq-id-mode 2 --similarity-type 2'.format(args.dev_dir, fasta, outfile.replace('.fasta', ''), args.work_dir, threads), stdout=subprocess.DEVNULL, shell=True)
	child.wait()
	# Remove tmp directory
	child = subprocess.Popen("rm -r {}tmp".format(args.work_dir), shell=True)
	child.wait()
	# Remove unwanted space characters added in fasta ids by linclust
	child = subprocess.Popen("sed  \"s/  //g\" {}_rep_seq.fasta > {}".format(outfile.replace('.fasta', ''), outfile), shell=True)
	child.wait()

def abundancy(clust_file, clust_type):
	if os.stat(clust_file).st_size != 0: # Check if abund file exists and is not empty

		if clust_type == 0: # For cdhit output handling
			with open(clust_file) as c:
				representants = dict()
				represented = defaultdict(list)
				for line in c:
					line = line.rstrip('\n')
					if line[0] == '>':
						cluster = int(line.split(' ')[1])
					elif re.search('\\*', line):
						representants[cluster] = line.split('>')[1].split('.')[0]
					else:
						represented[cluster].append(line.split('>')[1].split('.')[0])
			output = open('{}'.format(args.outfile.replace('.fasta', '.abund')), 'w+')
			for i in range(cluster+1):
				if not represented[i]:
					output.write(str(representants[i])+'\t'+'NONE'+'\n')
				else:
					output.write(str(representants[i])+'\t'+str(','.join(represented[i]))+'\n')

		elif clust_type == 1: # For linclust output handling
			with open(clust_file) as c:
				cluster = defaultdict()
				for line in c:
					ls = line.rstrip('\n').split('\t')
					rept = ls[0]
					repd = ls[1]
					if rept not in cluster:
						cluster[rept] = ['NONE']
					elif cluster[rept] == ['NONE']:
						cluster[rept] = [repd]
					else:
						cluster[rept].append(repd)
			output = open('{}'.format(args.outfile.replace('.fasta', '.abund')), 'w+')
			for rept in cluster:
				output.write(str(rept)+'\t'+str(','.join(cluster[rept]))+'\n')

	else:
		output = open('{}'.format(args.outfile.replace('.fasta', '.abund')), 'w+')

if args.clust_prog == "cdhit":
	# Launch cdhit for clustering, with 97% id
	cdhit(args.fasta, args.outfile, args.threads)
	clust_type = 0
	# Name of cluster file to open for abundance generation
	clust_file = args.outfile+'.clstr'

elif args.clust_prog == "linclust":
	# Launch linclust for clustering, with 97% id
	linclust(args.fasta, args.outfile, args.threads)
	clust_type = 1
	# Name of cluster file to open for abundance generation
	clust_file = args.outfile.replace('.fasta', '_cluster.tsv')

else:
	print("No action done, verify input and options")

# Generate abundancy file
abundancy(clust_file, clust_type)