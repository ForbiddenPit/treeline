#!/usr/bin/python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

from Bio import SeqIO
from Bio import Phylo
import os
import re
import sys
import glob
import subprocess
import string
from collections import defaultdict
from collections import OrderedDict

# Open dictionary file
def open_dict(dict_file):

	print('opening dictionary file...')

	dic = dict()
	with open(dict_file) as d:
		for line in d:
			if re.match('^[0-9]', line):
				ls = line.strip().split('\t')
				derep_id = ls[0]
				original_id = ls[1].split(';')[0].split('_')[0]
				dic[derep_id] = original_id

	print('dictionary opened successfuly')
	return dic

# Retrieve original ids of a swarm fasta
def original_swarm_id(swarm_file, dic):
	output = open('{}/oid_{}'.format(os.path.dirname(swarm_file), os.path.basename(swarm_file)), 'w+')
	with open(swarm_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			output.write('>'+str(dic[record.id.split('_')[0]])+'\n'+str(record.seq)+'\n')

# Retrieve original ids of a fasta
def original_fasta_id(fasta_file, dic):
	output = open('{}/oid_{}'.format(os.path.dirname(fasta_file), os.path.basename(fasta_file)), 'w+')
	with open(fasta_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			output.write('>'+str(dic[record.id.split('_')[0]])+'\n'+str(record.seq)+'\n')

# Retrieve original ids of a nwk
def original_nwk_id(nwk_file, dic):
	newick_out = '{}/oid_{}'.format(os.path.dirname(nwk_file), os.path.basename(nwk_file))
	sequences_out = '{}/oid_sequences.list'.format(os.path.dirname(nwk_file))
	print('opening newick file')
	tree = Phylo.read(nwk_file, 'newick')
	print('changing nodes ids for original ids')
	with open(sequences_out, 'w') as sqo:
		for leaf in tree.get_terminals():
			leaf.name = dic[leaf.name]
			sqo.write(leaf.name+'\n')
	Phylo.write(tree, newick_out, 'newick')
	print('files '+newick_out+' and '+sequences_out+' produced')

# Retrieve original ids of a nwk
def nwk_ids_list(nwk_file):
	sequences_out = '{}/oid_sequences.list'.format(os.path.dirname(nwk_file))
	print('opening newick file')
	tree = Phylo.read(nwk_file, 'newick')
	print('changing nodes ids for original ids')
	with open(sequences_out, 'w') as sqo:
		for leaf in tree.get_terminals():
			sqo.write(leaf.name+'\n')
	print(sequences_out+' produced')

# Crate new unique fasta ids and a global dictionnary
def rename_original_fasta(fasta_dir, output_dir, data_type):
	print('renaming initial fastas...')
	seq_count = 1

	if data_type == 'ref':
		# prefix = '0'
		suffix = '0'
	elif data_type == 'env':
		# prefix = '1'
		suffix = '1'
	else:
		sys.exit('please relaunch with the good data type (env or ref)')

	for fasta in glob.glob(fasta_dir+'*.fasta'):
		output1 = open('{}/renamed_{}'.format(os.path.dirname(output_dir), os.path.basename(fasta)), 'w+')
		output2 = open('{}/renamed_{}'.format(os.path.dirname(output_dir), os.path.basename(fasta).replace('.fasta', '.dict')), 'w+')
		with open(fasta) as f:
			for record in SeqIO.parse(f, 'fasta'):
				output1.write(">"+str(seq_count)+suffix+'\n'+str(record.seq)+'\n')
				output2.write(str(seq_count)+suffix+'\t'+str(record.description)+'\n')
				seq_count += 1
		print(os.path.basename(fasta))

	print('done')

# Check cdhit abundancy
def cdhit_abundancy(cluster_file):
	if os.stat(cluster_file).st_size != 0:
		with open(cluster_file) as c:
			representants = dict()
			represented = defaultdict(list)
			for line in c:
				line = line.rstrip('\n')
				if line[0] == '>':
					cluster = int(line.split(' ')[1])
				elif re.search('\\*', line):
					representants[cluster] = line.split('>')[1].split('.')[0]
				else:
					represented[cluster].append(line.split('>')[1].split('.')[0])
		output = open('{}/{}'.format(os.path.dirname(cluster_file), os.path.basename(cluster_file).replace('.fasta.clstr', '.abund')), 'w+')
		for i in range(cluster+1):
			if not represented[i]:
				output.write(str(representants[i])+'\t'+'NONE'+'\n')
			else:
				output.write(str(representants[i])+'\t'+str(','.join(represented[i]))+'\n')
	else:
		output = open('{}/{}'.format(os.path.dirname(cluster_file), os.path.basename(cluster_file).replace('.fasta.clstr', '.abund')), 'w+')

# Check edges abundancy
def edges_abundancy(edges_file):
	if os.stat(edges_file).st_size != 0:
		cc_found = False
		with open(edges_file) as e:
			representants = dict()
			represented = defaultdict(list)
			for line in e:
				line = line.rstrip('\n')
				if line.startswith('>'):
					cc_found = True
					cluster = int(line.split('CC')[1])-1
				elif re.search('\\*', line):
					representants[cluster] = line.strip('\\*')
				elif cc_found == True:
					represented[cluster].append(line)
				else:
					represented[0] = ''
					cluster = 0
		output = open('{}/{}'.format(os.path.dirname(edges_file), os.path.basename(edges_file).replace('.edges', '.abund')), 'w+')
		for i in representants:
			if not represented[i]:
				output.write(str(representants[i])+'\t'+'NONE'+'\n')
			else:
				output.write(str(representants[i])+'\t'+str(','.join(represented[i]))+'\n')
	else:
		output = open('{}/{}'.format(os.path.dirname(edges_file), os.path.basename(edges_file).replace('.edges', '.abund')), 'w+')

# Create a blast id for each line of a fasta before launching a blast
def blast_id(fasta_file):
	print('writing output blastid and count table files...')
	output1 = open('{}/{}'.format(os.path.dirname(fasta_file), os.path.basename(fasta_file).replace('oid_', 'blastid_')), 'w+')
	output2 = open('{}/{}'.format(os.path.dirname(fasta_file), os.path.basename(fasta_file).replace('oid_', 'count_').replace('.fasta', '.tsv')), 'w+')
	count_table = 0
	with open(fasta_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			output1.write('>'+str(count_table)+'\n'+str(record.seq)+'\n')
			output2.write(str(count_table)+'\t'+str(record.description)+'\n')
			count_table += 1
	print('done')

# Check if vxtractor result has duplicates and keep longest sequence of two
def check_duplicates(fasta_file):
	fasta_size = subprocess.check_output('wc -l {}'.format(fasta_file), shell=True, universal_newlines=True)
	fasta_size = int(fasta_size.split()[0]) / 2
	print('checking duplicates in fasta file and keeping longest sequence of two...')
	found_seqs = []
	num_seq = 0
	num_dup = 0
	new_fasta = OrderedDict()
	with open(fasta_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			num_seq += 1
			seq_id = record.id.split('_')[0]
			if seq_id not in found_seqs:
				found_seqs.append(seq_id)
				new_fasta[seq_id] = record.seq
			elif seq_id in found_seqs:
				if len(record.seq) > len(new_fasta[seq_id]):
					# print('found biggest duplicate for '+seq_id+':')
					# print('old seq: '+str(new_fasta[seq_id])+'\nnew seq: '+str(record.seq))
					# print('old seq len: '+str(len(new_fasta[seq_id]))+'\tnew seq len: '+str(len(record.seq)))
					new_fasta[seq_id] = record.seq
					num_dup += 1
				else:
					continue
			else:
				continue
			percent_complete = num_seq / fasta_size * 100
			sys.stdout.write('Percent complete:\t'+str(round(percent_complete))+'%\r')
			sys.stdout.flush()
	print('\ntotal number of sequences: '+str(num_seq))
	print('number of duplicates deleted: '+str(num_dup))

	with open('nodup_{}'.format(fasta_file), 'w') as o:
		for seq in new_fasta:
			o.write('>'+str(seq)+'\n'+str(new_fasta[seq])+'\n')

# Create only a count the count table
def update_count_table(fasta_file, count_table):
	seq_list = []
	with open(fasta_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			seq_list.append(record.id)
	print('comparing fasta to count table\nand writing output count table file...')
	output = open('{}/count_{}'.format(os.path.dirname(fasta_file), os.path.basename(fasta_file).replace('.fasta', '.tsv')), 'w+')
	with open(count_table) as c:
		for line in c:
			ls = line.split('\t')
			if ls[0] in seq_list:
				output.write(line)
			else:
				continue
	print('done')

# Find taxonomy in swarm files
def swarm_handling(swarm_file, dic, taxo):
	print('writing output taxo file...')
	output = open('otu_taxo_{}'.format(os.path.basename(swarm_file).strip('.fasta')), 'w')
	with open(swarm_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			swarm_id = record.id.split('_')[0]
			output.write('>'+str(swarm_id)+': '+taxo[dic[swarm_id]]+'\n')
	print('done')

# Create dictionary for tara sequences (no annotation)
def tara_dict(tara_fasta):
	print('reading tara fasta file...')
	output = open('tara_all.dict', 'w')
	with open(tara_fasta) as t:
		for record in SeqIO.parse(t, 'fasta'):
			output.write(record.id + '\t' + record.id + '\t' + 'marine metagenome;unclassified sequences' + '\n')

# Open taxonomy file for gg
def taxo_gg(taxo_file):

	print('opening gg taxo file...')

	taxo = dict()
	list_taxo = set()
	with open(taxo_file) as t:
		for line in t:
			ls = line.split('\t')
			taxo_id = ls[0].rstrip('\n')
			whole_taxo = ls[2].split(';')
			taxonomy = whole_taxo[0].replace('k__', '')+';'+whole_taxo[1].replace('p__', '')+';'+whole_taxo[2].replace('c__', '')
			taxo[taxo_id] = taxonomy
			list_taxo.add(taxo_id)

	print('gg taxonomy opened successfuly')
	return taxo, list_taxo

# Open taxonomy file for cpr_dpann
def taxo_other(taxo_file):

	print('opening other taxo file...')

	taxo = dict()
	list_taxo = set()
	with open(taxo_file) as t:
		for line in t:
			ls = line.split('\t')
			taxo_id = ls[0].rstrip('\n')
			whole_taxo = ls[2].rstrip('\n').split(';')
			if len(whole_taxo) == 0:
				taxonomy = 'Unknown;Unknown;Unknown'
			elif len(whole_taxo) == 1:
				taxonomy = whole_taxo[0]+';Unknown;Unknown'
			elif len(whole_taxo) == 2:
				taxonomy = whole_taxo[0]+';'+whole_taxo[1]+';Unknown'
			else:
				taxonomy = whole_taxo[0]+';'+whole_taxo[1]+';'+whole_taxo[2]
			taxo[taxo_id] = taxonomy
			list_taxo.add(taxo_id)

	print('other taxonomy opened successfuly')
	return taxo, list_taxo

# Retrive taxonomy for a dict file
def dict_taxo(dict_file, taxo_file):
	print('creating new taxo file from dictionary...')
	output = open('{}/taxo_{}'.format(os.path.dirname(dict_file), os.path.basename(dict_file)), 'w')
	data_name = input('name of the dataset:\n\t1 = Greengenes\n\t2 = Environment\n\t3 = CPR_DPANN\nX = ? ')
	if data_name == '1':
		taxo_name = 'Greengenes'
		taxo, list_taxo = taxo_gg(taxo_file)
	elif data_name == '2':
		taxo_name = input('name of the environmental dataset? ')
		taxo, list_taxo = taxo_other(taxo_file)
	elif data_name == '3':
		taxo_name = 'CPR_DPANN'
		taxo, list_taxo = taxo_other(taxo_file)

	with open(dict_file) as c:
		for line in c:
			ls = line.split()
			if data_name == '1':
				dict_id = ls[1].split('_')[0]
			elif data_name == '2':
				dict_id = ls[1].split('_')[0]
			elif data_name == '3':
				dict_id = ls[1].split('_')[0]
			if dict_id in list_taxo:
				whole_taxo = taxo[dict_id].split(';')
				t1 = whole_taxo[0]
				t2 = whole_taxo[1]
				t3 = whole_taxo[2]
				if data_name == '3':
					if whole_taxo[0] == 'Bacteria':
						# t1 = 'CPR'
						taxo_name = 'CPR'
						if re.search(r'bacterium', t3):
							t3 = re.match(r"(.*?)+bacterium", t3).group()
							if re.search(r'TPA_asm: ', t3):
								t3 = t3.replace('TPA_asm: ', '')
						elif re.search(r'Candidate division TM7', t3):
							t3 = 'Candidate division TM7 bacterium'
					elif whole_taxo[0] == 'Archaea':
						# t1 = 'DPANN'
						taxo_name = 'DPANN'
						if re.search(r'archaeon', t3):
							t3 = re.match(r"(.*?)+archaeon", t3).group()
							if re.search(r'TPA_asm: ', t3):
								t3 = t3.replace('TPA_asm: ', '')
					else:
						# t1 = 'CPR_DPANN'
						taxo_name = 'CPR_DPANN'
				if not t1:
					t1 = 'Unknown'
				if not t2:
					t2 = 'Unknown'
				if not t3:
					t3 = 'Unknown'
				output.write(ls[0]+'\t'+t1+'\t'+t2+'\t'+t3+'\t'+taxo_name+'\n')
			else:
				output.write(ls[0]+'\t'+taxo_name+'\t'+taxo_name+'\t'+taxo_name+'\t'+taxo_name+'\n')
	print('done')

# Generate abundance outer ring
def generate_abundance(derep_dir, cdhit_dir, network_dir, fasta_file):

	final_abund = dict()
	with open(fasta_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			final_abund[record.id] = 1

	derep_abund = dict()
	derep_files = glob.glob(derep_dir+'*.abund')
	for file in derep_files:
		with open(file) as derep:
			for line in derep:
				ls = line.split()
				d_id = ls[0]
				d_ab = ls[1]
				derep_abund[d_id] = d_ab

	cdhit_abund = dict()
	cdhit_files = glob.glob(cdhit_dir+'*.abund')
	for file in cdhit_files:
		with open(file) as cdhit:
			for line in cdhit:
				ls = line.split()
				c_id = ls[0]
				c_ab = ls[1].split(',')
				cdhit_abund[c_id] = c_ab

	network_abund = dict()
	network_files = glob.glob(network_dir+'*.abund')
	for file in network_files:
		with open(file) as network:
			for line in network:
				ls = line.split()
				n_id = ls[0]
				n_ab = ls[1].split(',')
				network_abund[n_id] = n_ab

	for f_id in final_abund:
		if f_id in network_abund:
			for c_id in network_abund[f_id]:
				if c_id == "NONE":
					continue
				else:
					if c_id in cdhit_abund:		
						for d_id in cdhit_abund[c_id]:
							if d_id == "NONE":
								continue
							else:
								if d_id in derep_abund:
									final_abund[f_id] += int(derep_abund[d_id])
								else:
									continue
					else:
						continue
		else:
			continue

	output = open('final_cc_blastid_all-merged.abund', 'w')
	for n_id in final_abund:
		output.write(str(n_id)+'\t'+str(final_abund[n_id])+'\n')

def gbff_handling(gbff_dir):

	def write_fasta(gbff_file, output_name):
		output1 = open('{}.fna'.format(on), 'a')
		output2 = open('{}.dict'.format(on), 'a')
		with open(gbff_file) as g:
			for record in SeqIO.parse(g, 'gb'):
				output1.write('>'+str(record.id)+'\n'+str(record.seq)+'\n')
				output2.write(str(record.id) + '\t' + str(record.name) + '\t' + str(';'.join(record.annotations['taxonomy'])) + '\n')

	on = input('New fasta and dict name: ')
	num_found = len(glob.glob(gbff_dir))
	print('number of files found in folder: ' + str(num_found))
	num_treated = 0
	for f in glob.glob(gbff_dir):
		num_treated += 1
		percent_complete = str(round(num_treated / num_found * 100, 2))
		sys.stdout.write('Percent complete:\t'+percent_complete+'\r')
		sys.stdout.flush()
		write_fasta(f, on)

# USELESS: blast 'filtering' and handling
# def blast_handling(blast_file, taxo, list_taxo):

# 	def coverage(start, end, length):
# 		return round(((end - start + 1) / length * 100), 2)

# 	output = open('taxo_{}'.format(os.path.basename(blast_file)), 'w')
# 	line_count = 0

# 	with open(blast_file) as f:
# 		for line in f:

# 			line_count += 1

# 			ls = line.split()
# 			qStart = float(ls[5])
# 			qEnd = float(ls[6])
# 			qLen = float(ls[7])
# 			sStart = float(ls[8])
# 			sEnd = float(ls[9])
# 			sLen = float(ls[10])

# 			qCov = coverage(qStart, qEnd, qLen)
# 			sCov = coverage(sStart, sEnd, sLen)

# 			# if qCov > 80 and sCov > 80:

# 			id1 = ls[0]
# 			id2 = ls[1]
# 			eVal = ls[2]
# 			pid = round(float(ls[3]), 1)

# 			if id1 in list_taxo:
# 				taxo1 = taxo[id1]
# 			else:
# 				taxo1 = 'dallol'

# 			if id2 in list_taxo:
# 				taxo2 = taxo[id2]
# 			else:
# 				taxo2 = 'dallol'

# 			sys.stdout.write('Treating line:\t'+str(line_count)+'\r')
# 			sys.stdout.flush()

# 			output.write(id1+'\t'+id2+'\t'+taxo1+'\t'+taxo2+'\t'+eVal+'\t'+str(pid)+'\t'+str(qCov)+'\t'+str(sCov)+'\n')

# Retrieve taxonomy to paste it next to blast ids for network
# def taxo_network(count_table, all_taxo, all_lists):

# 	if len(all_taxo) == 1:
# 		print('Creating taxo table for gg...')
# 		output = open('{}/taxo_{}'.format(os.path.dirname(count_table), os.path.basename(count_table)), 'w')
# 		meta_t = input('name of the metagenome: ')
# 		with open(count_table) as c:
# 			for line in c:
# 				ls = line.split()
# 				if ls[1] in all_lists[0]:
# 					whole_taxo = all_taxo[0][ls[1]].split(';')
# 					t1 = whole_taxo[0]
# 					t2 = whole_taxo[1]
# 					if not t2:
# 						t2 = 'Unknown'
# 					output.write(ls[0]+'\t'+t1+'\t'+t2+'\n')
# 				else:
# 					output.write(ls[0]+'\t'+meta_t+'\t'+meta_t+'\n')

# 	if len(all_taxo) == 2:
# 		print('creating taxo table for gg & cpr_dpann...')
# 		output = open('{}/taxo_{}'.format(os.path.dirname(count_table), os.path.basename(count_table)), 'w')
# 		meta_t = input('name of the metagenome: ')
# 		with open(count_table) as c:
# 			for line in c:
# 				ls = line.split()
# 				if ls[1] in all_lists[0]:
# 					whole_taxo = all_taxo[0][ls[1]].split(';')
# 					t1 = whole_taxo[0]
# 					t2 = whole_taxo[1]
# 					if not t2:
# 						t2 = 'Unknown'
# 					output.write(ls[0]+'\t'+t1+'\t'+t2+'\n')
# 				elif ls[1] in all_lists[1]:
# 					whole_taxo = all_taxo[1][ls[1]].split(';')
# 					if whole_taxo[0] == 'Bacteria':
# 						t1 = 'CPR'
# 					elif whole_taxo[0] == 'Archaea':
# 						t1 = 'DPANN'
# 					else:
# 						t1 = 'Unknown CPR/DPANN'
# 					t2 = whole_taxo[1]
# 					if not t2:
# 						t2 = 'Unknown'
# 					output.write(ls[0]+'\t'+t1+'\t'+t2+'\n')
# 				else:
# 					output.write(ls[0]+'\t'+meta_t+'\t'+meta_t+'\n')

# 	if len(all_taxo) == 3:
# 		print('Creating taxo table for gg & cpr_dpann & other...')
# 		output = open('{}/taxo_{}'.format(os.path.dirname(count_table), os.path.basename(count_table)), 'w')
# 		meta_t = input('name of the metagenome: ')
# 		other_t = input('name of the other dataset: ')
# 		with open(count_table) as c:
# 			for line in c:
# 				ls = line.rstrip('\n').split('\t')
# 				if ls[1] in all_lists[0]:
# 					whole_taxo = all_taxo[0][ls[1]].split(';')
# 					t1 = whole_taxo[0]
# 					t2 = whole_taxo[1]
# 					if not t2:
# 						t2 = 'Unknown'
# 					output.write(ls[0]+'\t'+t1+'\t'+t2+'\n')
# 				elif ls[1] in all_lists[1]:
# 					whole_taxo = all_taxo[1][ls[1]].split(';')
# 					if whole_taxo[0] == 'Bacteria':
# 						t1 = 'CPR'
# 					elif whole_taxo[0] == 'Archaea':
# 						t1 = 'DPANN'
# 					else:
# 						t1 = 'Unknown CPR/DPANN'
# 					t2 = whole_taxo[1]
# 					if not t2:
# 						t2 = 'Unknown'
# 					output.write(ls[0]+'\t'+t1+'\t'+t2+'\n')
# 				#########################################################################################
# 				# THIS PART OF THE CODE IS SPECIFIC TO 8000 DATASET WHICH HAS THE SAME IDS AS CPR DPANN #
# 				#########################################################################################
# 				elif re.search('SSU', ls[1]) and ls[1].split(' ')[0] in all_lists[2]:
# 					whole_taxo = all_taxo[2][ls[1].split(' ')[0]].split(';')
# 					t2 = whole_taxo[1]
# 					if not t2:
# 						t2 = 'Unknown'
# 					output.write(ls[0]+'\t'+other_t+'\t'+t2+'\n')
# 				#########################################################################################
# 				# ##################################################################################### #
# 				#########################################################################################
# 				#########################################################################################
# 				# THIS PART OF THE CODE IS SPECIFIC TO TARA DATASET 									#
# 				#########################################################################################
# 				elif re.search('ENA', ls[1]) and ls[1] in all_lists[2]:
# 					whole_taxo = all_taxo[2][ls[1].split(' ')[0]].split(';')
# 					t2 = whole_taxo[1]
# 					if not t2:
# 						t2 = 'Unknown'
# 					output.write(ls[0]+'\t'+other_t+'\t'+t2+'\n')
# 				#########################################################################################
# 				# ##################################################################################### #
# 				#########################################################################################
# 				# elif ls[1] in all_lists[2]:
# 				# 	whole_taxo = all_taxo[2][ls[1]].split(';')
# 				# 	t2 = whole_taxo[1]
# 				# 	if not t2:
# 				# 		t2 = 'Unknown'
# 				# 	output.write(ls[0]+'\t'+other_t+'\t'+t2+'\n')
# 				else:
# 					output.write(ls[0]+'\t'+meta_t+'\t'+meta_t+'\n')

# def unique_ids_in_8000_g(id1, id2, fasta_file):
# 	id1_list = set()
# 	with open(id1) as i1:
# 		for line in i1:
# 			id1_list.add(line.rstrip('\n'))

# 	id2_list = set()
# 	with open(id2) as i2:
# 		for line in i2:
# 			id2_list.add(line.rstrip('\n'))

# 	output = open('16s_8000_all.fna', 'w')
# 	found_ids = set()
# 	for id1 in id1_list:
# 		with open(fasta_file) as f:
# 			for record in SeqIO.parse(f, 'fasta'):
# 				if re.search(id1, record.id) and id1 not in found_ids:
# 					output.write('>'+str(record.description)+'\n'+str(record.seq)+'\n')
# 					found_ids.add(id1)
# 					print(id1)
# 	for id2 in id2_list:
# 		with open(fasta_file) as f:
# 			for record in SeqIO.parse(f, 'fasta'):
# 				if re.search(id2, record.id) and id2 not in found_ids:
# 					output.write('>'+str(record.description)+'\n'+str(record.seq)+'\n')
# 					found_ids.add(id2)
# 					print(id2)
