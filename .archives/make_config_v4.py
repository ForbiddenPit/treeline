#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import yaml
import argparse
import sys
import math

## Main
parser = argparse.ArgumentParser()

# Directories
parser.add_argument('-w', '--workdir', metavar='DIR', help='working directory', default='./trees/', required=True, dest='work_dir')
parser.add_argument('-d', '--devdir', metavar='DIR', help='treeline repository', default='./treeline/', required=True, dest='dev_dir')

# Computer params
parser.add_argument('-t', '--max_threads', metavar='NUM', help='number of threads (should be superior to files number to haste computation)', default='1', required=False, dest='max_threads', type=int)
parser.add_argument('-f', '--max_files', metavar='NUM', help='greatest number of files (of all subprojects)', default='1', required=False, dest='max_files', type=int)
parser.add_argument('-m', '--max_mem', metavar='NUM', help='maximum virtual memory to allocate', default='50g', required=False, dest='max_mem')

# Project name
parser.add_argument('-r', '--ref_name', metavar='STR', help='name of the reference data', default='', required=True, dest='ref_name')
parser.add_argument('-e', '--env_name', metavar='STR', help='name of the environmental data', default='', required=True, dest='env_name')

# Alphabet choice
# parser.add_argument('--alignment_type', metavar='STR', help='type of alignment (blast, mmseq2 or diamond)', default='blast', required=True, dest='align_type')
# parser.add_argument('--data_type', metavar='STR', help='type of data (dna or aa)', default='dna', required=True, dest='data_type')
parser.add_argument('--alignment_type', metavar='STR', help='type of alignment (n or a)', default='n', required=True, dest='align_type')

# Family Detector params
parser.add_argument('-fd', '--familydetector', metavar='NUM', help='pid threshold for familydetector (95 = genus; 97 = species)', default='95', required=False, dest='familydetector')

# jD2Stats params
parser.add_argument('-a', '--analysis', metavar='STR', help='k-mers method to use (D2S is advised when n = 0; D2N is advised when n > 0)', default='D2S', required=False, dest='analysis_type')
parser.add_argument('-k', '--kmers', metavar='NUM', help='number of k-mers', default='5', required=False, dest='kmers')
parser.add_argument('-n', '--neighbor', metavar='NUM', help='number of neighbors', default='0', required=False, dest='neighbor')

args = parser.parse_args()

# Threads number calculated in function of number of maximum input files
max_threads = args.max_threads
max_files = args.max_files
if max_threads <= max_files:
    cdhit_threads = str(math.ceil(max_threads/max_files))
elif max_threads > max_files:
    cdhit_threads = str(max_threads//max_files)
max_mem = args.max_mem+'g'

# Alignment type
align_type = args.align_type
if align_type == "n":
    db_type = "nucl"
    alphabet = "dna"
elif align_type == "a":
    db_type = "prot"
    alphabet = "aa"

# Other options
familydetector = args.familydetector
analysis_type = args.analysis_type
kmers = args.kmers
neighbor = args.neighbor

# Working directory
work_dir = os.path.abspath(args.work_dir)
if not os.path.exists(work_dir):
    os.makedirs(work_dir)
else:
    print("Working directory "+work_dir+" already exists")

# Create config directory if it doesn't exist
ref_name = args.ref_name
env_name = args.env_name
config_dir = work_dir+'/config/'+env_name+'-'+ref_name+'/'
if not os.path.exists(config_dir):
    os.makedirs(config_dir)
else:
    print("Config directory "+config_dir+" already exists")

# Create only one config file in project directory
output_file = config_dir+'fd'+familydetector+'_k'+kmers+'_n'+neighbor+'.yaml'
dev_dir = os.path.abspath(args.dev_dir)+'/'
env_dir = work_dir+'/data/env/'+env_name+'/'
env_res_dir = work_dir+'/results/env/'+env_name+'/'
ref_dir = work_dir+'/data/ref/'+ref_name+'/'
ref_res_dir = work_dir+'/results/ref/'+ref_name+'/'
common_res_dir = work_dir+'/results/common/'+env_name+'-'+ref_name+'/'
tree_params = "_fd"+familydetector+"_k"+kmers+"_n"+neighbor
config_file = {
"dev_dir": dev_dir,
"env_name": env_name,
"env_dir": env_dir,
"env_res_dir": env_res_dir,
"ref_name": ref_name,
"ref_dir": ref_dir,
"ref_res_dir": ref_res_dir,
"common_res_dir": common_res_dir,
"tree_params": tree_params,
"cdhit": {"c":"0.97", "n":"10", "d":"0", "M":"16000", "T":str(cdhit_threads)}, 
"makeblastdb": {"dbtype":db_type},
"blast": {"blast_type":align_type,"num_threads":str(max_threads), "max_target_seqs":"5000", "soft_masking":"false"},
"familydetector": {"p":familydetector, "c":"80", "m":"cc", "o":"1", "t":str(max_threads)},
"jD2Stat": {"max_mem":max_mem, "k":kmers, "d":analysis_type, "t":str(max_threads), "a":alphabet, "n":neighbor}
}
with open(output_file, 'w') as outfile:
    yaml.dump(config_file, outfile, default_flow_style=False, sort_keys=False)
print("Config file "+os.path.basename(output_file)+" created")

# Create other directories if it doesn't exist
results_dir = work_dir+'/results/'
if not os.path.exists(results_dir):
    os.makedirs(env_res_dir)
    os.makedirs(ref_res_dir)
data_dir = work_dir+'/data/'
if not os.path.exists(data_dir):
    os.makedirs(env_dir)
    os.makedirs(ref_dir)
else:
	pass














# Create one config file for each subproject in project directory
# if subproject > 0:
#     for i in range(0, subproject):
#         subproject_name = input('Enter n°{} project\'s name: '.format(i+1))
#         output_file = config_dir+subproject_name+'_config.yaml'
#         env_dir = work_dir+'/data/env/'+project_name+'/'+subproject_name+'/'
#         ref_dir = work_dir+'/data/ref/'+ref_name+'/'
#         env_res_dir = work_dir+'/results/env/'+project_name+'/'+subproject_name+'/'
#         ref_res_dir = work_dir+'/results/ref/'+ref_name+'/'
#         tree_name = subproject_name+"_k"+kmers+"_n"+neighbor
#         config_file = {
#         "dev_dir": dev_dir,
#         "env_dir": env_dir,
#         "ref_dir": ref_dir,
#         "results_dir": results_dir,
#         "tree_name": tree_name,
#         "cdhit": {"c":"0.97", "n":"10", "d":"0", "M":"16000", "T":str(cdhit_threads)}, 
#         "makeblastdb": {"dbtype":db_type},
#         "blast": {"blast_type":blast_type, "num_threads":str(max_threads), "max_target_seqs":"5000", "soft_masking":"false"},
#         "familydetector": {"p":familydetector, "c":"80", "m":"cc", "o":"1", "t":str(max_threads)},
#         "jD2Stat": {"max_mem":max_mem, "k":kmers, "d":analysis_type, "t":str(max_threads), "a":alphabet, "n":neighbor}
#         }
#         with open(output_file, 'w') as outfile:
#             yaml.dump(config_file, outfile, default_flow_style=False, sort_keys=False)
#         print("Config file "+os.path.basename(output_file)+" created")

# # Create only one config file in project directory
# else:
#     output_file = config_dir+'config.yaml'
#     data_dir = work_dir+'/data/'+project_name+'/'
#     results_dir = work_dir+'/results/'+project_name+'/'
#     tree_name = "k"+kmers+"_n"+neighbor
#     config_file = {
#     "dev_dir": dev_dir,
#     "data_dir": data_dir,
#     "results_dir": results_dir,
#     "tree_name": tree_name,
#     "cdhit": {"c":"0.97", "n":"10", "d":"0", "M":"16000", "T":str(cdhit_threads)}, 
#     "makeblastdb": {"dbtype":db_type},
#     "blast": {"blast_type":blast_type,"num_threads":str(max_threads), "max_target_seqs":"5000", "soft_masking":"false"},
#     "familydetector": {"p":familydetector, "c":"80", "m":"cc", "o":"1", "t":str(max_threads)},
#     "jD2Stat": {"max_mem":max_mem, "k":kmers, "d":analysis_type, "t":str(max_threads), "a":alphabet, "n":neighbor}
#     }
#     with open(output_file, 'w') as outfile:
#         yaml.dump(config_file, outfile, default_flow_style=False, sort_keys=False)
#     print("Config file "+os.path.basename(output_file)+" created")