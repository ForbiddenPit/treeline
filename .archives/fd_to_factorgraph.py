#!/usr/bin/python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

import argparse
import subprocess
import os
import sys

import time
import re
import glob

parser = argparse.ArgumentParser(description='Script for alignment all vs all')

# Global arguments (start and end)
## I/O
parser.add_argument('--orig_dict', help='input dict file', required=True)
parser.add_argument('--new_dict', help='input nodes file', required=True)
parser.add_argument('--nodes', help='input nodes file', required=True)

args = parser.parse_args()

taxo = dict()
with open(args.orig_dict) as od:
	for line in od:
		ls = line.rstrip().split('\t')
		taxo[ls[0]] = ls[3]

old_ids = dict()
with open(args.new_dict) as nd:
	for line in nd:
		ls = line.rstrip().split('\t')
		old_ids[ls[0]] = ls[1]

with open(os.path.basename(args.nodes).replace('nodes', 'dict'), 'w') as output:
	with open(args.nodes) as n:
		for line in n:
			line = line.rstrip()
			if not re.search('>', line):
				# print(line)
				output.write(line+'\t'+taxo[old_ids[line]]+'\n')