import os
import re
import argparse
import ete3
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.colors as mptcol

class Node:

	def __init__(self, leaf_name):
		self.phylum = ""
		self.color = ""
		self.leaf_name = leaf_name
		self.type = ""
		self.tara = False
		self.ref = False
		self.cpr = False
		self.dpann = False
		self.env_70 = False
		self.env_90 = False
		self.cpr_dpann = None
		self.group = ""

file_ref_colour = "/home/romain/Projet/LouiseForTheWin/annot/reference_coulours.tsv"
dico = {}
with open(file_ref_colour) as f:

	for line in f:
		if line.strip():
			spt = line.strip().split()
			dico[spt[1]] = spt[-1]



def int_to_hexa(x):
	assert 0 <= x <= 255
	alpha = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]
	v = int(x / 16)
	b = int(x % 16)
	# print(v,b)
	return "{}{}".format(alpha[v], alpha[b])


def hexa_to_int(x):
	alpha = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]
	a = alpha.index(x[0])
	b = alpha.index(x[1])
	return a * 16 + b


def hexa__matpl(color):
	color = color.strip('#').lower()
	r1 = round(hexa_to_int(color[0:2]) / 255, 3)
	r2 = round(hexa_to_int(color[2:4]) / 255, 3)
	r3 = round(hexa_to_int(color[4:]) / 255, 3)
	return (r1, r2, r3)


def get_number_of_sequences_and_informative_sites(log_file):

	num_seq = re.compile("Alignment has (\d+) sequences")
	info = re.compile("(\d+) parsimony-informative")
	info_2 = re.compile("(\d+) informative sites")

	seq_number, site_informative = 0, 0
	with open(log_file) as log:
		for line in log:

			seq_num = num_seq.match(line)
			inf = info.match(line)
			info2 = info_2.search(line)
			if seq_num:
				seq_number = seq_num.group(1)
			if inf or info2:
				if info2:
					site_informative = info2.group(1)
				else:
					site_informative = inf.group(1)
				break
	if seq_number and site_informative :
		return seq_number, site_informative
	else:
		print("noinfo: {}".format(log_file))
		raise ArithmeticError

reg = re.compile("\s(\S+){superkingdom};\s((\w+|\s?)+){?;?")
dico_ref_annot = {}
annot_taxid = "/home/disque3To/Romain/Projet/LouiseForTheWin/annot/tax_id_ref_to_taxonomy.txt"
with open(annot_taxid) as f:
	for line in f:
		spt = line.strip().split('\t')
		m = reg.search(line)
		if not m:
			dico_ref_annot[spt[0]] = "unclassified sequence"
		else:
			if m.group(1) =="Viruses":
				dico_ref_annot[spt[0]] = "virus"
			elif m.group(2) == "environmental samples":
				if m.group(1) == "Bacteria":
					dico_ref_annot[spt[0]] = "unclassified Bacteria"
				elif m.group(1) == "Archaea":
					dico_ref_annot[spt[0]] = "unclassified Archaea"

			else:
				dico_ref_annot[spt[0]] = m.group(2)

print(dico_ref_annot)


dico_color = {}
dico_color["unclassified sequence"] = "#d3d3d3"
dico_color["virus"] = "#ff34b3"
dico_color["PU"] = "#CD8043" #"#d3d3d3"
dico_color["UO"] = "#808080" #FF0000"
dico_color["WUO"] = "#383838" #"#F35C28"
dico_color["CPR"] = "#e84ad6" #FFA500"
dico_color["DPANN"] = "#8c1aa0"

path_ref_col = "/home/disque3To/Romain/Projet/LouiseForTheWin/annot/reference_coulours.tsv"
with open(path_ref_col) as p:
	for l in p:
		spt = l.strip().split('\t')
		dico_color[spt[1]] = spt[2]


arbre_dir = "/home/romain/Projet/LouiseForTheWin/tree_out"


all_file = sorted([os.path.join(arbre_dir, elem) for elem in os.listdir(arbre_dir) if 'contree' in elem or "log" in elem])

print(all_file)
#all_file.remove("/home/romain/Projet/LouiseForTheWin/arbre_ok/K08093.fasta.log")
#all_file.remove("/home/romain/Projet/LouiseForTheWin/arbre_ok/K14083.fasta.log")


dico_cpr_dpann = {}
cpr_dpann_dico_file = "/home/romain/database/dpann_cpr/assemblyIdToTaxonomy_v2_uniq.tsv"

with open(cpr_dpann_dico_file) as f:

	for line in f:
		spt = line.strip().split("\t")
		taxo = spt[-1].split(';')[0:2]

		if taxo[1] == "Euryarchaeota":

			if "Candidatus Altiarchaeales" in spt[-1]:
				taxo[1] = "Ca. Altiarchaeales"

			elif "Candidatus Nanohaloarchaeota" in spt[-1]:
				taxo[1] = "Ca. Nanohaloarchaeota"

		taxo[1] = taxo[1].replace("Candidatus", "Ca.")
		dico_cpr_dpann[spt[1][0:6]] = taxo



file_list = []
cpt = 0
while cpt < len(all_file) - 1:
	f0 = all_file[cpt]
	f1 = all_file[cpt+1]
	if ".fasta_uniq.fa_uniq.fa" in f0:
		cpt += 2
		continue
	if os.path.basename(f0).split('_')[0] == os.path.basename(f1).split('_')[0]:
		print(f0, f1)
		file_list.append((f0, f1))
		cpt += 2
	else:

		cpt += 1


unidir = "/home/disque4To/Romain/Projet/louise_stage/unifile"


dico_id_to_tax_id = {}

with open(os.path.join(unidir, "new__withgood_columns")) as f:
	for l in f:
		spt = l.strip().split("\t")
		for elem in spt[1].split(","):

			dico_id_to_tax_id[elem] = spt[-1].split(";")


with open(os.path.join(unidir, "new_other_withgood_columns")) as f:
	for l in f:
		spt = l.strip().split("\t")
		for elem in spt[1].split(","):
			dico_id_to_tax_id[elem] = spt[-1].split(";")


dico_node = {}
for i in range(0, len(file_list)):
	tree_ = file_list[i][0]
	log = file_list[i][1]
	print(tree_, log)
	seq_number, site_informative = get_number_of_sequences_and_informative_sites(log)
	mytree = ete3.Tree(tree_)
	for leave in mytree.iter_leaves():
		name = leave.name
		my_node = Node(leaf_name=leave.name)
		if name.startswith('OM-RGC'):
			my_node.tara = True
		elif len(name.split('_')) == 2:
			my_node.cpr_dpann = True
			taxo_abbr = dico_cpr_dpann[name.split('_')[1][0:6]]
			if taxo_abbr[0] == "Bacteria":
				my_node.cpr = True
			else:
				my_node.dpann = True
			my_node.group = taxo_abbr[1]
		else:

			my_node.ref = True
			try:
				prot_id = name.split("_")[2]
			except:
				print(name, file_list)
				raise
			try:
				tax_id = dico_id_to_tax_id[prot_id]
			except:
				print(prot_id)
			try:
				annot = None
				color = None
				for tax in tax_id:
					annot = dico_ref_annot.get(tax.strip())
					if annot:
						color = dico_color[annot]
						break
				if not annot:
					print(prot_id)
					print(annot)
					print(color)
					raise AssertionError()
			except:
				print(tax_id, tax)
				print(leave.name)
				raise
			my_node.phylum = annot
			my_node.color = color

		dico_node[name] = my_node


# annot tara file
which_file = "/home/romain/Storage/UTS_METABOLISM_REVISION/which_in_which"
with open(which_file) as file_:
	for line in file_:
		spt = line.strip().split()

		if spt[0] in dico_node:
			node = dico_node[spt[0]]

			if "WUO" in spt:
				node.type = "WUO"
			elif "UO" in spt:
				node.type = "UO"
			else:
				node.type = "PU"

			if spt[1] == "None":
				node.env_70 = True

			else:

				if "WUO70" in spt or "UO70" in spt:
					node.env_70 = True

				elif "WUO90" in spt or "UO90" in spt:
					node.env_90 = True

cpr_dpann_dico_file = ""


all_ph = dico_color.keys()
def color_legend(dico_color: dict, list_here):

	key_list = list_here
	number_items = len(list_here)
	shape = ["2"] * number_items
	liste_color = []
	liste_labels = []
	#liste_order.extend(key_list)
	for key in list_here:
		value = dico_color[key]
		liste_color.append(value)
		liste_labels.append(key)

	return (shape, liste_color, liste_labels)


for i in range(0, len(file_list)):
	tree_ = file_list[i][0]
	log = file_list[i][1]
	my_tree = ete3.Tree(tree_)
	seq_numb, info_sites = get_number_of_sequences_and_informative_sites(log)
	base_file_name = os.path.join("/home/romain/Projet/LouiseForTheWin/arbre_annot", os.path.basename(tree_)) + str(seq_numb) + '_' + str(info_sites)
	leaf_name = []

	file_name = base_file_name + '_annotation_renaming.txt'
	with open(file_name, "w") as annot_file:

		annot_file.write("LABELS\n")
		annot_file.write("SEPARATOR TAB\n")
		annot_file.write("DATA\n")

		for leaf in my_tree.iter_leaves():
			leaf_name.append(leaf.name)
			node = dico_node[leaf.name]
			if node.leaf_name.startswith("OM-RGC"):
				value = node.type
			elif node.cpr_dpann:
				value = node.group

			elif node.ref:
				value = node.phylum

			annot_file.write("{}\t{}\n".format(leaf.name, "{}_{}".format(node.leaf_name, value)))

	shape, liste_color, liste_labels = color_legend(dico_color=dico_color, list_here=list(dico_color.keys()))
	file_name = base_file_name + '_annotation_colorBranches.txt'
	with open(file_name, 'w') as annot_file:
		annot_file.write("TREE_COLORS\n")
		annot_file.write("SEPARATOR TAB\n")
		annot_file.write("DATASET_LABEL\tcolorBranches\n")
		annot_file.write("COLOR\t#ff0000\n")
		annot_file.write("LEGEND_TITLE\t{}\n".format("Color Branches"))
		annot_file.write("LEGEND_COLORS\t{}\n".format('\t'.join(liste_color)))
		annot_file.write("LEGEND_LABELS\t{}\n".format('\t'.join(liste_labels)))
		annot_file.write("LEGEND_SHAPES\t{}\n".format('\t'.join(shape)))
		annot_file.write("DATA\n")

		for leaf in my_tree.iter_leaves():
			node = dico_node[leaf.name]
			# if node.cpr_dpann:
			# 	phyla = node.cpr_dpann
			# elif node.type:
			# 	phyla = node.type
			# else:
			# 	phyla = node.phylum
			print(node.__dict__)
			if node.ref:
				color = node.color
			else:
				if node.tara:
					color = dico_color[node.type]
				elif node.cpr_dpann:
					if node.cpr:
						color = dico_color["CPR"]
					else:
						color = dico_color["DPANN"]

			to_write = []
			to_write.append(leaf.name)
			to_write.append("clade")
			to_write.append(color)
			to_write.append("normal")
			to_write.append("1.0")
			annot_file.write('\t'.join(map(str, to_write))+'\n')

	#shape, liste_color, liste_labels = color_legend(dico_color=dico_annot_color, list_here=liste_phyla_here)
	file_name = base_file_name + '_annotation_colorLeafLabel.txt'

	with open(file_name, "w") as annot_file:
		annot_file.write("TREE_COLORS\n")
		annot_file.write("SEPARATOR TAB\n")
		annot_file.write("COLOR\t#ff0000\n")
		annot_file.write("DATASET_LABEL   colorLeafLabel\n")
		annot_file.write("LEGEND_TITLE\t{}\n".format("Color LeafLabel"))
		annot_file.write("LEGEND_SHAPES\t{}\n".format('\t'.join(shape)))
		annot_file.write("LEGEND_COLORS\t{}\n".format('\t'.join(liste_color)))
		annot_file.write("LEGEND_LABELS\t{}\n".format('\t'.join(liste_labels)))
		annot_file.write("DATA\n")

		for leaf in my_tree.iter_leaves():
			node = dico_node[leaf.name]
			if node.ref:
				color = node.color
			else:
				if node.tara:
					color = dico_color[node.type]
				elif node.cpr_dpann:
					if node.cpr:
						color = dico_color["CPR"]
					else:
						color = dico_color["DPANN"]

			font = "normal"

			if not leaf.name.startswith("OM-RGC"):
				size = "1"

			else:

				if node.env_70:
					font = "bold-italic"
					size = "1"

				elif node.env_90:
					font = "bold"
					size = "1"

				else:
					size = "1"
			to_write = [leaf.name, "label", color, font, size]
			annot_file.write('\t'.join(map(str, to_write)) + '\n')



	file_name = base_file_name + '_annotation_colorStrip_new.txt'

	with open(file_name, 'w') as annot_file:
		annot_file.write("DATASET_COLORSTRIP\n")
		annot_file.write("SEPARATOR TAB\n")
		#annot_file.write("COLOR_BRANCHES\t1\n")
		annot_file.write("DATASET_LABEL\tcolors_annotation\n")
		annot_file.write("COLOR\t#ff0000\n")
		annot_file.write("LEGEND_TITLE\t{}\n".format("Color chart"))
		annot_file.write("LEGEND_SHAPES\t{}\n".format('\t'.join(shape)))
		annot_file.write("LEGEND_COLORS\t{}\n".format('\t'.join(liste_color)))
		annot_file.write("LEGEND_LABELS\t{}\n".format('\t'.join(liste_labels)))
		annot_file.write("DATA\n")

		for leaf in my_tree.iter_leaves():
			node = dico_node[leaf.name]
			#phyla = node.phylum
			# if node.cpr_dpann:
			# 	phyla = node.cpr_dpann

			if not node.cpr_dpann and not node.tara:
				continue


			if node.tara:
				color = dico_color[node.type]
				phyla = node.type
			elif node.cpr_dpann:
				if node.cpr:
					color = dico_color["CPR"]
				else:
					color = dico_color["DPANN"]
				phyla = node.group

			to_write = [leaf.name, color, phyla]
			try:
				annot_file.write('\t'.join(map(str, to_write))+'\n')
			except:
				print(leaf.name, color, phyla)
				raise


	file_name = base_file_name + '_annotation_colorRange_Branches_new.txt'
	with open(file_name, 'w') as annot_file:
		annot_file.write("TREE_COLORS\n")
		annot_file.write("SEPARATOR TAB\n")

		annot_file.write("DATASET_LABEL\tcolorRangesBranches\n")
		annot_file.write("DATA\n")

		for leaf in my_tree.iter_leaves():
			node = dico_node[leaf.name]
			if node.ref:
				color = node.color
				phyla = node.phylum
			else:
				if node.tara:
					color = dico_color[node.type]
					phyla = node.type
				elif node.cpr_dpann:
					if node.cpr:
						color = dico_color["CPR"]
					else:
						color = dico_color["DPANN"]
					phyla = node.group
			to_write = []
			to_write.append(leaf.name)
			to_write.append("range")
			to_write.append(color)
			to_write.append(phyla)

			try:
				annot_file.write('\t'.join(map(str, to_write))+'\n')

			except:
				print(leaf.name, color, phyla)
				raise
