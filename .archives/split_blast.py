#!/usr/bin/python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

import argparse
import subprocess
import os

import time
import re
import glob
from multiprocessing import Pool

parser = argparse.ArgumentParser(description='Script for blast all vs all with nucleotides')

# Global arguments (start and end)
parser.add_argument('-f', help='input fasta', required=True)
parser.add_argument('-out', help='outfile', required=True)
# Fastasplit arguments
parser.add_argument('-s', help='split folder path', default='split')
parser.add_argument('-c', help='number of splits (= num_threads)', required=True, type=int)
# Blast arguments
parser.add_argument('-b', help='set blast type (n or p)', default='n')
parser.add_argument('-db', help='database for the blast')
parser.add_argument('-num_threads', help='number of threads')
parser.add_argument('-max_target_seqs', help='maximum target sequences')
parser.add_argument('-soft_masking', help='soft masking')

args = parser.parse_args()


def blast(tuple_args):
	"""Just launch the blast"""
	query, database, output_file = tuple_args
	cmd = 'blast{} -query {} -db {} -out {} -outfmt \"6 qseqid sseqid evalue pident bitscore qstart qend qlen sstart send slen\" -num_threads {} -max_target_seqs {} -soft_masking {}'.format(args.b, query, database, output_file, args.num_threads, args.max_target_seqs, args.soft_masking)
	try:
		child = subprocess.Popen(cmd, shell=True)
		child.wait()
	except:
		raise
	else:
		return 0

# Check number of sequences before split
n_seq = int(subprocess.check_output("grep -c '>' {}".format(args.f), shell=True).decode('utf-8'))

if args.c < n_seq:
	nb_splits = args.c
else:
	nb_splits = n_seq

# Create split folder if doesn't exist
if not os.path.exists(args.s):
    os.makedirs(args.s)

# Fastasplit
child = subprocess.Popen('fastasplit -f {} -o {} -c {}'.format(args.f, args.s, nb_splits),
                         stdout=subprocess.DEVNULL, shell=True)
child.wait()

# Find newly created splits
files = glob.glob(args.s+'*.fasta_*')

# pool.map: we create arguments for each file
list_args = list()
count = 0

for file in files:
	count += 1
	tuple_args = (file, args.db, file.replace('.fasta', '.out'))
	list_args.append(tuple_args)

# Create pool
with Pool(processes=args.c) as pool:
	# Launch processes
	out = pool.map(blast, list_args)

# Concatenate results
child = subprocess.Popen("cat {} > {}".format(args.s+'*.out_*', args.out), shell=True)
child.wait()

# Remove split directory
subprocess.Popen("rm -r {}".format(args.s), shell=True)