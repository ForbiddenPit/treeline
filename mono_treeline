#########################################################################
############################## TreeLine v4 ##############################
#########################################################################

import sys, re, glob, os, subprocess, errno, argparse

# Setup initial data directories and fasta files list
dev_dir = config["dev_dir"]
ref_dir = config["ref_dir"]
env_dir = config["env_dir"]
REF, = glob_wildcards(ref_dir+"/{refsample}.fasta")
ENV, = glob_wildcards(env_dir+"/{envsample}.fasta")

# Results directories
env_res_dir = config["env_res_dir"]
ref_res_dir = config["ref_res_dir"]
common_res_dir = config["common_res_dir"]

# Setup final tree name
env_name = config["env_name"]
ref_name = config["ref_name"]
tree_params = config["tree_params"]

# Create an empty environment file if only reference is processed
env = config["env"]
if env == 0 and not os.path.exists(env_dir+"empty_env.fasta"):
	with open(env_dir+"empty_env.fasta", "w"): pass
	suffix = "only_reference/"
	tree_name = ref_name+tree_params
elif env == 0 and os.path.exists(env_dir+"empty_env.fasta"):
	suffix = "only_reference/"
	tree_name = ref_name+tree_params
elif env == 1:
	suffix = "reference_environment/"
	tree_name = env_name+"-"+ref_name+tree_params

# Other parameters for each rule
clust_params = config["clustering"]
align_params = config["align"]
cleanblast_params = config["cleanblast"]
familydetector_params = config["familydetector"]
jD2Stat_params = config["jD2Stat"]

# Global pseudo rule #########################################################################
## Used to tell the snakemake pipeline what to produce
rule all:
	input:
		ref_res_dir+"ssn/cc_ref-merged.abund",
		env_res_dir+"ssn/cc_env-merged.abund" if env == 1 else env_res_dir+"ssn/empty_env.fasta",
		common_res_dir+suffix+tree_name+".nwk",
		# common_res_dir+suffix+"reality_check/CC.edges",
		common_res_dir+suffix+tree_name+".params"

# Reference handling #########################################################################
## All reference rules, always used in the pipeline

# Rename initial fastas to have uniform ids, lighter for blast and big data handling
rule ref_rename:
	input:
		r = expand(ref_dir+"{refsample}.fasta", refsample=REF)
	output:
		f = expand(ref_res_dir+"renamed/renamed_{refsample}.fasta", refsample=REF),
		d = expand(ref_res_dir+"renamed/renamed_{refsample}.dict", refsample=REF)
	shell: """
		cd {ref_res_dir}renamed/
		python3 {dev_dir}scripts/16s_handling.py -fd {ref_dir} -o {ref_res_dir}renamed/ --data ref
		"""

# First clustering with 100% identity
rule ref_dereplicate:
	input:
		f = ref_res_dir+"renamed/renamed_{refsample}.fasta"
	output:
		f = ref_res_dir+"derep/derep_{refsample}.fasta",
		a = ref_res_dir+"derep/derep_{refsample}.abund"
	shell: """
		cd {ref_res_dir}derep/
		python3 {dev_dir}scripts/derep_seqs.py -i {input.f} -f {output.f} -d {output.a}
		"""

# Second clustering with 97% identity (can use either cdhit or linclust)
rule ref_clustering:
	input:
		rules.ref_dereplicate.output.f
	output:
		ref_res_dir+"clust/clust_{refsample}.fasta"
	params:
		threads = clust_params['num_threads'],
		clust_prog = clust_params['clust_prog']
	shell: """
		{dev_dir}scripts/clustering.py --fasta {input} --outfile {output} --threads {params.threads} --clust_prog {params.clust_prog} --work_dir {ref_res_dir}clust/ --dev_dir {dev_dir}
		"""

rule ref_merge:
	input:
		r_f = expand(rules.ref_clustering.output, refsample = REF),
		r_d = expand(ref_res_dir+"renamed/renamed_{refsample}.dict", refsample = REF)
	output:
		r_f = ref_res_dir+"align/ref-merged.fasta",
		r_d = ref_res_dir+"align/ref-merged.dict"
	run:
		subprocess.call("cat {} > {}".format(input.r_f, output.r_f), shell=True)
		subprocess.call("cat {} > {}".format(input.r_d, output.r_d), shell=True)

# Align #########################################################################

rule ref_align:
	input:
		fasta = rules.ref_merge.output.r_f
	output:
		outfile = ref_res_dir+"align/ref-merged.out"
	params:
		align_prog = align_params["align_prog"],
		align_type = align_params["align_type"],
		num_threads = align_params["num_threads"],
		dbtype = align_params["dbtype"]
	shell: """
		{dev_dir}scripts/alignment.py --fasta {input.fasta} --output {output.outfile} --align_prog {params.align_prog} --align_type {params.align_type} --threads {params.num_threads} --dbtype {params.dbtype} --work_dir {ref_res_dir}align/ --dev_dir {dev_dir}
		"""

# SSN #########################################################################

rule ref_cleanblast:
	input:
		rules.ref_align.output
	output:
		ssn = ref_res_dir+"ssn/ref-merged.out.cleanNetwork",
		genes = ref_res_dir+"ssn/ref-merged.out.cleanNetwork.genes"
	params:
		m = cleanblast_params["multiplicator"]
	shell: """
		{dev_dir}scripts/MultiTwin/BlastProg/cleanblast -i {input} -d {ref_res_dir}ssn/ -s {params.m}
		"""

rule ref_familydetector:
	input:
		ssn = rules.ref_cleanblast.output.ssn,
		genes = rules.ref_cleanblast.output.genes
	output:
		ref_res_dir+"ssn/CC.edges"
	params:
		"-p "+familydetector_params["p"]+" -c "+familydetector_params["c"]+" -m "+familydetector_params["m"]+" -o "+familydetector_params["o"]+" -t "+familydetector_params["t"]
	shell: """
		{dev_dir}scripts/MultiTwin/BlastProg/familydetector -i {input.ssn} -n {input.genes} -d {ref_res_dir}ssn/ {params}
		"""

rule ref_java_scripts:
	input:
		f = ref_res_dir+"align/ref-merged.fasta",
		e = rules.ref_familydetector.output
	output:
		new_edges = ref_res_dir+"ssn/cc_ref-merged.edges",
		new_fasta = ref_res_dir+"ssn/cc_ref-merged.fasta"
	shell: """
		cd {ref_res_dir}ssn/
		java -jar {dev_dir}scripts/Scripts_jar/Scripts.jar {input.f} {input.e} {output.new_edges} {output.new_fasta}
		"""

rule ref_edges_abundancy:
	input:
		rules.ref_java_scripts.output.new_edges
	output:
		ref_res_dir+"ssn/cc_ref-merged.abund"
	shell: """
		cd {ref_res_dir}ssn/
		{dev_dir}scripts/16s_handling.py -e {input}
		"""


if env == 1:
	# Environment handling #########################################################################

	rule env_rename:
		input:
			e = expand(env_dir+"{envsample}.fasta", envsample=ENV)
		output:
			f = expand(env_res_dir+"renamed/renamed_{envsample}.fasta", envsample=ENV),
			d = expand(env_res_dir+"renamed/renamed_{envsample}.dict", envsample=ENV)
		shell: """
			cd {env_res_dir}renamed/
			python3 {dev_dir}scripts/16s_handling.py -fd {env_dir} -o {env_res_dir}renamed/ --data env
			"""

	rule env_dereplicate:
		input:
			f = env_res_dir+"renamed/renamed_{envsample}.fasta"
		output:
			f = env_res_dir+"derep/derep_{envsample}.fasta",
			a = env_res_dir+"derep/derep_{envsample}.abund"
		shell: """
			cd {env_res_dir}derep/
			python3 {dev_dir}scripts/derep_seqs.py -i {input.f} -f {output.f} -d {output.a}
			"""

	rule env_clustering:
		input:
			rules.env_dereplicate.output.f
		output:
			env_res_dir+"clust/clust_{envsample}.fasta",
		params:
			threads = clust_params['num_threads'],
			clust_prog = clust_params['clust_prog']
		shell: """
			{dev_dir}scripts/clustering.py --fasta {input} --outfile {output} --threads {params.threads} --clust_prog {params.clust_prog} --work_dir {env_res_dir}clust/ --dev_dir {dev_dir}
			"""

	rule env_merge:
		input:
			e_f = expand(rules.env_clustering.output, envsample = ENV),
			e_d = expand(env_res_dir+"renamed/renamed_{envsample}.dict", envsample = ENV)
		output:
			e_f = env_res_dir+"align/env-merged.fasta",
			e_d = env_res_dir+"align/env-merged.dict",
		run:
			subprocess.call("cat {} > {}".format(input.e_f, output.e_f), shell=True)
			subprocess.call("cat {} > {}".format(input.e_d, output.e_d), shell=True)

	# Blast #########################################################################
	
	rule env_align:
		input:
			fasta = rules.env_merge.output.e_f
		output:
			outfile = env_res_dir+"align/env-merged.out"
		params:
			align_prog = align_params["align_prog"],
			align_type = align_params["align_type"],
			num_threads = align_params["num_threads"],
			dbtype = align_params["dbtype"]
		shell: """
			{dev_dir}scripts/alignment.py --fasta {input.fasta} --output {output.outfile} --align_prog {params.align_prog} --align_type {params.align_type} --threads {params.num_threads} --dbtype {params.dbtype} --work_dir {env_res_dir}align/ --dev_dir {dev_dir}
			"""

	# SSN #########################################################################

	rule env_cleanblast:
		input:
			rules.env_align.output
		output:
			ssn = env_res_dir+"ssn/env-merged.out.cleanNetwork",
			genes = env_res_dir+"ssn/env-merged.out.cleanNetwork.genes"
		params:
			m = cleanblast_params["multiplicator"]
		shell: """
			{dev_dir}scripts/MultiTwin/BlastProg/cleanblast -i {input} -d {env_res_dir}ssn/ -s {params.m}
			"""

	rule env_familydetector:
		input:
			ssn = rules.env_cleanblast.output.ssn,
			genes = rules.env_cleanblast.output.genes
		output:
			env_res_dir+"ssn/CC.edges"
		params:
			"-p "+familydetector_params["p"]+" -c "+familydetector_params["c"]+" -m "+familydetector_params["m"]+" -o "+familydetector_params["o"]+" -t "+familydetector_params["t"]
		shell: """
			{dev_dir}scripts/MultiTwin/BlastProg/familydetector -i {input.ssn} -n {input.genes} -d {env_res_dir}ssn/ {params}
			"""

	rule env_java_scripts:
		input:
			f = env_res_dir+"align/env-merged.fasta",
			e = rules.env_familydetector.output
		output:
			new_edges = env_res_dir+"ssn/cc_env-merged.edges",
			new_fasta = env_res_dir+"ssn/cc_env-merged.fasta"
		shell: """
			cd {env_res_dir}ssn/
			java -jar {dev_dir}scripts/Scripts_jar/Scripts.jar {input.f} {input.e} {output.new_edges} {output.new_fasta}
			"""

	rule env_edges_abundancy:
		input:
			rules.env_java_scripts.output.new_edges
		output:
			env_res_dir+"ssn/cc_env-merged.abund"
		shell: """
			cd {env_res_dir}ssn/
			{dev_dir}scripts/16s_handling.py -e {input}
			"""

elif env == 0:

	if not os.path.exists(env_res_dir+"ssn/"):
		os.makedirs(env_res_dir+"ssn/")
		print("Directory "+env_res_dir+"ssn/ created ")
	else:
		print("Directory "+env_res_dir+"ssn/ already exists")

	if not os.path.exists(env_res_dir+"ssn/empty_env.fasta"):
		with open(env_res_dir+"ssn/empty_env.fasta", 'w') as o:
			print('No env selected, generated empty_env.fasta for replacement. It will be used to produce a tree with reference sequences only')
	else: pass


# Check if data is too big before clustering env #########################################################################

# rule check_size:
# 	input:
# 		r_f = rules.ref_merge.output.r_f,
# 		e_f = rules.ref_merge.output.e_f
# 	output:
# 		ifs = "checked.txt"
# 	run:
# 		ref_fasta_size = subprocess.check_output('wc -l {}'.format(input.r_f), shell=True, universal_newlines=True)
# 		ref_fasta_size = int(ref_fasta_size.split()[0]) / 2
# 		env_fasta_size = subprocess.check_output('wc -l {}'.format(input.e_f), shell=True, universal_newlines=True)
# 		env_fasta_size = int(env_fasta_size.split()[0]) / 2
# 		if (ref_fasta_size + env_fasta_size) > 80000:
# 			with open(res_dir+"blast/use_reduced_env.txt", 'w'):
# 				print("data too big, will use reduced_env-merged.fasta for blast", ref_fasta_size, env_fasta_size)
# 		else:
# 			with open(res_dir+"blast/use_whole_env.txt", 'w'):
# 				print("data is fine, will use whole_env-merged.fasta for blast", ref_fasta_size, env_fasta_size)
# 		with open(str(output.ifs), 'w'):
# 			print("checked")

rule merge_all:
	input:
		r_f = ref_res_dir+"ssn/cc_ref-merged.fasta",
		e_f = env_res_dir+"ssn/cc_env-merged.fasta" if env == 1 else env_res_dir+"ssn/empty_env.fasta"
	output:
		f = common_res_dir+suffix+"cc_all-merged.fasta"
	run:
		print('Collating env and ref new fastas...')
		print('.....')
		subprocess.call("cat {} {} > {}".format(input.r_f, input.e_f, output.f), shell=True)
		print('Done')

# When calling TreeLine for other jD2Stat options, will start from here #########################################################################

rule jD2Stat:
	input:
		f = common_res_dir+suffix+"cc_all-merged.fasta"
	output:
		common_res_dir+suffix+tree_name+".phy"
	params:
		max_mem = "-Xmx"+jD2Stat_params["max_mem"]+"g",
		other = "-k "+jD2Stat_params["k"]+" -d "+jD2Stat_params["d"]+" -t "+jD2Stat_params["t"]+" -a "+jD2Stat_params["a"]+" -n "+jD2Stat_params["n"]
	shell: """
		cd {common_res_dir}{suffix}
		java -jar {params.max_mem} {dev_dir}scripts/jD2Stat_2_0_jar/jD2Stat_2.0.jar -i {input.f} -o {output} {params.other}
		"""

# Final result #########################################################################

rule rapidNJ:
	input:
		rules.jD2Stat.output
	output:
		common_res_dir+suffix+tree_name+".nwk"
	shell: """
		cd {common_res_dir}{suffix}
		{dev_dir}scripts/rapidNJ/bin/rapidnj {input} -x {output}
		sed \"s/'//g\" {output} > {output}.tmp
		mv {output}.tmp {output}
		"""

# Merge initial ref #########################################################################

# rule initial_ref_merge:
# 	input:
# 		f = expand(ref_res_dir+"derep/derep_{refsample}.fasta", refsample = REF),
# 		ssn = common_res_dir+suffix+tree_name+".nwk"
# 	output:
# 		m = common_res_dir+suffix+"reality_check/derep_ref-merged.fasta"
# 	shell: """
# 		cat {input.f} > {output.m}
# 		"""

# Create initial ref blast database #########################################################################

# rule initial_ref_db:
# 	input:
# 		common_res_dir+suffix+"reality_check/derep_ref-merged.fasta"
# 	output:
# 		common_res_dir+suffix+"reality_check/derep_ref-merged_database.nhr" if align_params["align_type"] == "n" else common_res_dir+suffix+"reality_check/derep_ref-merged_database.phr",
# 		common_res_dir+suffix+"reality_check/derep_ref-merged_database.nin" if align_params["align_type"] == "n" else common_res_dir+suffix+"reality_check/derep_ref-merged_database.pin",
# 		common_res_dir+suffix+"reality_check/derep_ref-merged_database.nsq" if align_params["align_type"] == "n" else common_res_dir+suffix+"reality_check/derep_ref-merged_database.psq"
# 	params:
# 		dbtype = config["makeblastdb"]["dbtype"]
# 	shell: """
# 		makeblastdb -in {input} -dbtype {params.dbtype} -out {common_res_dir}{suffix}reality_check/derep_ref-merged_database
# 		"""

# Reality check #########################################################################

# rule reality_check:
# 	input:
# 		e_f = env_res_dir+"ssn/cc_env-merged.fasta" if env == 1 else env_res_dir+"ssn/empty_env.fasta",
# 		r_db = rules.initial_ref_db.output
# 	output:
# 		common_res_dir+suffix+"reality_check/CC.edges"
# 	params:
# 		align_type = align_params["align_type"],
# 		c = align_params["num_threads"],
# 		num_threads = align_params["num_threads"],
# 		max_target_seqs = align_params["max_target_seqs"],
# 		soft_masking = align_params["soft_masking"],
# 		fd = "-p 99"+" -c "+familydetector_params["c"]+" -m "+familydetector_params["m"]+" -o "+familydetector_params["o"]+" -t "+familydetector_params["t"]
# 	shell: """
# 		{dev_dir}scripts/alignment.py -f {input.e_f} -s {common_res_dir}{suffix}reality_check/split/ -b {params.align_type} -c {params.c} -db {common_res_dir}{suffix}reality_check/derep_ref-merged_database -out {common_res_dir}{suffix}reality_check/rc.out -num_threads {params.num_threads} -max_target_seqs {params.max_target_seqs} -soft_masking {params.soft_masking}
# 		{dev_dir}scripts/MultiTwin/BlastProg/cleanblast -i {common_res_dir}{suffix}reality_check/rc.out -d {common_res_dir}{suffix}reality_check/
# 		{dev_dir}scripts/MultiTwin/BlastProg/familydetector -i {common_res_dir}{suffix}reality_check/rc.out.cleanNetwork -n {common_res_dir}{suffix}reality_check/rc.out.cleanNetwork.genes -d {common_res_dir}{suffix}reality_check/ {params.fd}
# 		"""

# Parameters used print #########################################################################

rule parameters:
	input:
		rules.rapidNJ.output
	output:
		common_res_dir+suffix+tree_name+".params"
	run:
		print('|||||||||||||||||||| Treeline finished all tasks with these parameters: ||||||||||||||||||||	\n')
		print('	results directory: '+common_res_dir+suffix)
		print('\n	parameters file: '+tree_name+"_params.log")
		print('\n	tree file: '+tree_name+'.nwk')
		print('\n	clustering:\n'+'    		%: '+clust_params["clust_prog"])
		print('\n	familydetector:\n'+'    		pid: '+familydetector_params["p"]+'\n'+'    		coverage: '+familydetector_params["c"])
		print('\n	jD2Stat:\n'+'    		maximum ram: '+jD2Stat_params["max_mem"]+'\n'+'    		analysis: '+jD2Stat_params["d"]+'\n'+'    		kmers: '+jD2Stat_params["k"]+'\n'+'    		neighbors: '+jD2Stat_params["n"])
		print('\n||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||	\n')
		output = open(common_res_dir+suffix+tree_name+".params", 'w')
		output.write('|||||||||||||||||||| Parameters ||||||||||||||||||||	\n\n')
		output.write('	results directory: '+common_res_dir+suffix+'\n')
		output.write('\n	tree file: '+tree_name+'.nwk'+'\n')
		output.write('\n	clustering:\n'+'    		%: '+clust_params["clust_prog"]+'\n')
		output.write('\n	familydetector:\n'+'    		pid: '+familydetector_params["p"]+'\n'+'    		coverage: '+familydetector_params["c"]+'\n')
		output.write('\n	jD2Stat:\n'+'    		maximum ram: '+jD2Stat_params["max_mem"]+'\n'+'    		analysis: '+jD2Stat_params["d"]+'\n'+'    		kmers: '+jD2Stat_params["k"]+'\n'+'    		neighbors: '+jD2Stat_params["n"]+'\n')
		output.write('\n||||||||||||||||||||||||||||||||||||||||||||||||||||	\n')
